//
//  SAMacros.swift
//  SmartApp
//
//  Created by Karen Sidney on 16/09/17.
//  Copyright © 2017 SURA. All rights reserved.
//
//  Macro para imprimir logs solo en degub


import Foundation

  //  Imprimir strings solo en debug
  func DLog(message: String, filename: String = #file, function: String = #function, line: Int = #line) {
    #if DEBUG
        
        let url = URL(fileURLWithPath: filename)
        let className:String! = url.lastPathComponent
        print("[\(className!):\(line)] \(function) - \(message)")

    #endif
  }



