//
//  Constants.swift
//  SmartApp
//
//  Created by Karen Sidney on 06/08/17.
//  Copyright © 2017 SURA. All rights reserved.
//  Constantes que se utilizan el todo el proyecto

import Foundation
import UIKit

  // MARK: Nómina por default
  public let kSAUserDefault:String = "1016386"
  public let kSASmartAppTrackingID_DEV: String = "UA-117172145-1"
  public let kSASmartAppTrackingID_PROD: String = "UA-117262051-1"


  // MARK: Tabla coroporativa
  public let kA: String = "A"  // A: Tabla de usuarios
  public let kA1: String = "A1"  // A1: Constante para la Nomina del usuario logueado
  public let kA2: String = "A2"  // A2: Constante para saber si el usuario esta logueado
  public let kA3: String = "A3"  // A3: Constante para la nomina del usuario que se usará en búsqueda
  public let kA4: String = "A4"  // A4: Constante MX
  public let kA5: String = "A5"  // A4: Constante IPAddress con la que se logueo
  public let kA6: String = "A6"  // A4: "Constante token con el que se logueo
  public let kIsLoggedIn: String = "1"  // 1:Valor que indica Usuario logueado

  public let kB: String = "B"  // B: Tabla de periodo
  public let kB1: String = "B1"  // B1: Constante del periodo seleccionado
  public let kB2: String = "B2"  // B1: Constante del año seleccionado
  public let kB3: String = "B3"  // B1: Constante de la fecha inicio seleccionado
  public let kB4: String = "B4"  // B1: Constante de la fecha fin seleccionado

  public let kNameApp: String = "SmartApp"
  public let kDesactivateApp: Int = 0 //Desactivo
  public let kActivateApp: Int = 1 //activo
  public let kiOSApp: Int = 2 // iOS

  // MARK: - Modificadores de  pago
  public let SAModificadorPago120 = "1,20 X"
  public let SAModificadorPago105 = "1,05 X"
  public let SAModificadorPago100 = "1,00 X"
  public let SAModificadorPago190 = "0,90 X"

  // MARK: - Indicadores y semaforos
  public let SAIndicatorRedMax:Double = 69.9
  public let SAIndicatorRedMin:Double = 0.0
  public let SAIndicatorYellowMax:Double = 89.9
  public let SAIndicatorYellowMin:Double = 70.0
  public let SAIndicatorGreenMax:Double = 90.0

  public let SASemaphoreRedMax:CGFloat = 9.9
  public let SASemaphoreRedMin:CGFloat  = 0.0
  public let SASemaphoreYellowMax:CGFloat = 69.9
  public let SASemaphoreYellowMin:CGFloat = 10.0
  public let SASemaphoreOrangeMax:CGFloat = 89.9
  public let SASemaphoreOrangeMin:CGFloat = 70.0
  public let SASemaphoreGreenMax:CGFloat = 90.0


  public let SASemaphoreResultsExamRedMax:CGFloat = 59.9
  public let SASemaphoreResultsExamRedMin:CGFloat = 0.0

  public let SASemaphoreResultsExamYellowMax:CGFloat = 84.9
  public let SASemaphoreResultsExamYellowMin:CGFloat = 60.0
  public let SASemaphoreResultsExamGreenMin:CGFloat = 85.0
  public let SASemaphoreResultsExamGreenMax:CGFloat = 100.0

  // MARK: - Regresar a determinada posición
  public let SATagBackChecklist:Int = 1
  public let SATagBackAccompaniment:Int = 2
  public let SATagBackSession1a1:Int = 3
  public let SATagBackBlackboard:Int = 4
  public let SATagBackGalery:Int = 5
  public let SATagBackTracing:Int = 6
  public let SATagBackToSendEmailAgain:Int = 7

  // MARK: - Request - Respuesta

  public let SAResponseCode: String = "codigo"
  public let SAResponseMessage: String = "mensaje"
  public let SAResponseEmailCode: String = "code"
  public let SAResponseEmailMessage: String = "mesage"
  public let SAResponseMessageSuccess: String = "Success"

  public let SAResponsePlayload: String = "playload"
  public let SAResponseDescriptionCode: String = "descriptionCode"
  public let SAResponseStatusCode: String = "statusCode"
  public let SAResponseDPStatusCode: String = "estado"
  public let SAResponseDescription: String = "descripcion"

  //MARK: - Personal Goal
  public let kSAMessageConsultingGoals: String = "Message_ConsultingGoals"
  public let kSAMessageConsultingGoal: String = "Message_ConsultingGoal"
  public let kSAMessageUpdatingGoal: String = "Message_UpdatingGoal"
  public let kSAMessageSaveGoal: String = "Message_SaveGoal"


  // MARK: - Clase Login
  public let SAStoryboardMain: String =  "Main"
  public let SAStoryboardMainGeneric: String =  "Main_Generic"
  public let SALoginSignupVC : String = "LoginSignupVC"
  public let SAAnimation_duration: CGFloat = 0.5
  public let SASpace: CGFloat = 15
  public let SAAxisXZero: CGFloat = 0.0
  public let SAAxisYZero: CGFloat = 0.0
  public let SAFontSize17: CGFloat = 17.0
  public let SALengthTextZero: Int = 0
  public let SALengthTextSix: Int = 6
  public let SALengthTextThirteen: Int = 13
  public let SALengthTextFifteen: Int = 15
  public let SACornerRadius8: Int = 8
  public let SABorderWidthTextField: CGFloat = 1.0
  public let SAIDAccess: String = "19"
  public let kSANotAccessLogin:String = "Title_Not_Access"

  // MARK: - Clase SATools
  public let SAAplha: CGFloat = 1.0
  public let SAZeroOptions: Int = 0
  public let SARangeZero: Int = 0
  public let SAHASH: String = "#"
  public let SAStringFF: String = "ff"
  public let SALocaleMX: String = "es_MX"
  public let SASymbolMoney: String = "$"
  public let SAManager: String = "GERENTE"
  public let SAFarmer: String = "FARMER"
  public let SADirectorChannel: String = "DIRECTOR_CANAL"

  public let SAPositionExecutive: String = "EJECUTIVO"
  public let SAHunterPrevisional: String = "HUNTER PREVISIONAL"


  public let SACommercialManager: String = "Gte. Comercial"
  public let SAPrime: String = "Prime"
  public let SAHighHeritage: String = "Alto Patrimonio"
  public let SAHighValue: String = "Alto Valor"
  public let SAElite: String = "Elite"
  public let SAElitePremier: String = "Elite Premier"
  public let SAPremier: String = "Premium"
  public let SAInitialWithMX: String = "MX"


  // MARK: - Colores
  public let SANavigationBarBlueColorCode: String = "0033A0"
  public let SATableCellIconsBlueColorCode: String = "00226C"
  public let SATextNameBlackColorCode: String = "637283"
  public let SATextSecondaryBlackColorCode: String = "798CA7"

  public let SAGenericBlueColorOfLineOrButton: String = "00AEC7"
  public let SASelectedColorCellMenu: String = "00AEC7"
  public let SABorderColorButtonSelectedOptionTwo: String = "00AEC7"
  public let SAColorViewInformationExecutive: String = "53C8D9"
  public let SABackgroundColorTextView: String = "F7F9FA"

  public let SABackgroundColorGrayMainMenu: String = "DEE5E8"
  public let SABackgroundColorGrayLineForSeparateView: String = "DEE5E8"
  public let SABackgroundColorViewLeader: String = "E4F3FA"
  public let SABorderColorButtonSelected: String = "007E96"
  public let SAColorBottonFinishBlackboard: String = "00226C"
  public let SAColorCircleProgress: String = "0033A0"
  public let SAColorCircleNormal: String = "DEE5E8"
  public let SAColorSearch: String = "EEEEEE"
  public let SAColorRED: String = "FF0000"
  public let SAColorYELLOW: String = "ECDF36"
  public let SAColorGREEN: String = "7ED321"
  public let SAColorBackgroundPopover: String = "F4F6F7"
  public let SAColorF7F9FA: String = "F7F9FA"
  public let SAColorPink: String = "F798CA7"
  public let SAColorSemaphoreRed:String = "D0021B"
  public let SAColorSemaphoreYellow:String = "F8E71C"
  public let SAColorSemaphoreGreen:String = "7ED321"
  public let SAColorSemaphoreGray:String = "9A9A9A"
  public let SAColorSemaphoreBlue:String = "4A90E2"
  public let SAColorSemaphoreOrange:String = "F5A623"



  // MARK: - Colors
  public let SAColorFF6160:String = "FF6160" // red
  public let SAColor00226C: String = "00226C"
  public let SAColor3E4C5B: String = "3E4C5B"
  public let SAColor526376: String = "526376"
  public let SAColorB8C2DB: String = "B8C2DB"
  public let SAColor002B86: String = "002B86"
  public let SAColorE3E829: String = "E3E829" //amarillo
  public let SAColor00AEC7: String = "00AEC7"
  public let SAColor004BEC: String = "004BEC"
  public let SAColor003AB8: String = "003AB8"
  public let SAColor001F60: String = "001F60"
  public let SAColorDEE5E8: String = "DEE5E8"
  public let SAColorD0011B: String = "D0011B" // rojo
  public let SAColorF5A623: String = "F5A623" //Naranja
  public let SAColor4A90E2: String = "4A90E2" // azul
  public let SAColor71B225: String = "71B225" // Verde
  public let SAColor9014FE: String = "9014FE" // morado
  public let SAColor0033A0: String = "0033A0"
  public let SAColor7897BB: String = "7897BB"
  public let SAColor4F4F4F: String = "4F4F4F"
  public let SAColor4E6C9F: String = "4E6C9F"
  public let SAColorBACDDA: String = "BACDDA" // azul palido
  public let SAColor9C9F1F: String = "9C9F1F" // amarillo

  public let SAColorNameBLUE: String = "BLUE"
  public let SAColorNameRED: String = "RED"
  public let SAColorNameYELLOW: String = "YELLOW"
  public let SAColorNameORANGE: String = "ORANGE"
  public let SAColorNameGREEN: String = "GREEN"



  // MARK: - Notificaciones
  public let SANotificationVarShowMenu: String = "hideOrShowMenuLateral"
  public let SANotificationVarOptionMenu: String = "OptionList"
  // MARK: - Versión
  public let SAVersionApp: String = "CFBundleShortVersionString"

  // MARK: - Clase Selector
  public let SANavigationSelectorVC : String = "NavigationDateSelectorVC"
  public let SASegueSelectorViewController: String = "showSelectorViewController"
  public let SACellOptionsDate: String  = "OptionsDate"

  // MARK: - Clase Container
  public let SAMainGenericContainerVC: String = "MainGenericContainerVC"
  public let SAMainContainerVC: String = "MainContainerVC"
  public let SASegueContainerViewController: String = "showContainerViewController"
  public let SACellMenu: String = "MenuItemCell"

  // MARK: - Contenedor Principal
  public let kSAContainerMainDashboardOption: String = "MainMenuViewSegue"
  public let kSAContainerBlackboardOption: String = "BlackboardSegue"
  public let kSAContainerGaleryOption: String = "GalerySegue"
  public let kSAMainEmbedSegueProfile: String = "EmbedSegueProfile"
  public let kSAMainEmbedSegueGeneric: String = "EmbedSegue"
  public let kSAMainEmbedSegueMenuLateral: String = "EmbedSegueMenuLateral"


  // MARK: - Clase Dashboard
  public let SASegueContainerPhoneViewController: String = "showContainerPhone"
  public let kSAStoryboardDashboard: String  = "Dashboard"
  public let kSADashboardGenericVC: String  = "DashboardCVGeneric"

  public let kSAccompanimentSegue: String  = "showAccompaniment"
  public let kSASession1to1Segue: String  = "showSession1to1"
  public let kSAServiceSurveySegue: String  = "showServiceSurvey"
  public let kSASCheckListSegue: String  = "showChecklist"
  public let kSADevelopmentPlan: String = "showDevelopmentPlan"
  public let kSAGenericIntroductionSegue: String  = "Generic_Introduction"
  public let kSASearchTableViewSegue: String  = "showSearchView"

  public let kSADashboardMenuOptions: String  = "DashboardMenu"
  public let kSADashboardMenuPrincipalOptions: String  = "DashboardMenuPrincipal"
  public let kSABinnacleDashboardOptions: String = "BinnacleDashboard"
  public let kSANameViewHome: String = "View_Home"

  // MARK: - Clase Galería
  public let kSGaleryAllFilesSegue: String = "ShowAllImages"
  public let kSAPreviewViewController = "SAPreviewVC"
  public let kSAGaleryStoryboard = "Galery"
  public let kSAGaleryViewControllerID =  "GaleryVC"


  // MARK: - Llaves de fuentes
  public let kSAFamilyFontDINMedium = "DIN" 
  public let kSAFamilyFontDINRegular = "DIN-Regular"
  public let kSAFamilyFontDINBold = "DIN-Bold"
  public let kSAFamilyFontDINLight = "DIN-Light"

  // MARK: - Llaves de imágenes
  public let kSAImageBannerDashboard = "ImageDashboard"



  public let kSAImageCornerBottomLeft = "corner_bottom_left"
  public let kSAImageCornerTopLeft = "corner_top_left"
  public let kSAImageCornerTopRight = "corner_top_right"
  public let kSAImageCornerBottomRight = "corner_bottom_right"
  public let kSAImageClose = "close"
  public let kSAImagePathDropDown = "PathDropdown"

  // MARK: - Llaves de imágenes en el archivo .plist
  public let kSAImageDeactivated = "deactivated_image"
  public let kSAImageActivate = "activate_image"

  // MARK: - Llaves de imágenes y títulos en Dashboard
  public let kSAImageDashboardItem = "image_main_menu"
  public let kSATitleDashboardItem = "title_main_menu"
  public let kSACollectionCellDashboard = "MainMenuItemCell"
  public let kSAHeaderViewDashboard = "HeaderView"
  public let kSAIndentifierDashboardGenericCell:String = "SADashboardGenericCollectionViewCell"

  // MARK: - Llaves de celdas y vistas en Galery
  public let kSACollectionCellAlbum = "CellAlbum"
  public let kSAHeaderViewGalery = "HeaderViewGalery"
  public let kSACollectionCellImage = "CellImage"

  // MARK: - Llaves de celdas y vistas en Calendario
  public let kSACollectionViewAnnualCalendary = "CollectionViewAnnualCalendary"
  public let kSAHeaderViewAnnualCalendary = "HeaderViewAnnualCalendary"
  public let kSACollectionCellAnnualCalendary = "CellPeriodAnnualCalendary"
  public let SAShowAnnualCalendarySegue = "showListPeriods"
  // MARK: - Llaves de celdas y vistas en busqueda
  public let SASearchCellIdentifier = "SearchCell"

  // MARK: - Llaves de Archivos de configuración
  public let kSASmartAppID = "SmartAppID-ApplicationUniqueIdentifier"
  public let kSAConfiguration = "Configuration"
  public let kSAFileInfo = "Info"
  public let kSAExtensionPLIST = "plist"
  public let kSASimulation = "Simulation"
  public let kSAProduction = "Production"
  public let kSADevelopment = "Development"
  public let kSAEnvironments = "Environments"
  public let kSAAPIPoint = "APIPoint"
  public let kSAServices = "Services"
  public let kSAServiceLogin = "Login"
  public let kSAServicePeriod = "Period"
  public let kSAAPIPointProduction = "APIPointProduction"
  public let kSAAPIPointPush = "APIPointPush"
  public let kSAAPIPointPushSura = "APIPointPushSura"
  public let KSAAPISharePoint = "APISharePoint"
  public let kSAServiceAnnualReport = "AnnualReport"
  public let kSAServiceExecutive = "Executive"
  public let kSAQuestionAccompaniment = "QuestionsAccompaniment"
  public let kSAQuestionCheckList = "QuestionsCheckList"
  public let kSAQuestionServiceSurvey = "QuestionsServiceSurvey"
  public let kSASaveResponseOfQuestions = "SaveResponse"
  public let kSALogout = "Logout"
  public let kSAGetQuestions = "Questions"
  public let kSalesPeriodVision = "SalesPeriodVision"
  public let kSASendEmail = "SendEmail"
  public let kSAPushRegistry = "PushRegistry"
  public let kSAPushRegistrySura = "PushRegistrySura"
  public let kSAStatusPush = "PushStatus"
  public let kSASaveImageProfile = "SaveImageProfile"
  public let kSAConsultImageProfile = "ConsultImageProfile"
  public let kSADeleteImageProfile = "DeleteImageProfile"
  public let kSAPlan = "planes_ejecutivos"
  public let KSAExtensionCSV = "csv"
  public let kSASavePersonalGoal = "SavePersonalGoal"
  public let kSAConsultPersonalGoals = "ConsultPersonalGoals"
  public let kSAConsultSpecificGoal = "ConsultSpecificGoal"
  public let kSAUpdatePersonalGoal = "UpdatePersonalGoal"
  public let kSADataLinking = "DataLinking"
  public let KSADevelopmentPlan = "DevelopmentPlan"
  public let KSAHistoricServiceSurvey = "HistoricServiceSurvey"
  public let KSAHistoricAccompaniment = "HistoricAccompaniment"
  public let KSAHistoricCheckList = "HistoricCheckList"
  public let KSAHistoricSessionTo1 = "HistoricSessionTo1"
  public let KSAGroupMeeting = "GroupMeetingSharePoint"
  public let kSASharePoint = "sharePointId"

  // MARK: - Llaves de celdas para la vista Plan de Desarrollo
  public let kSAAccompanimentCell = "accompanimentCell"
  public let KSAMonthtlyGoalCell = "monthlyGoalCell"
  public let kSASession1and1Cell = "session1and1Cell"
  public let kSAServiceSurveyCell = "serviceSurveyCell"


  // MARK: - Llaves de uso genérico
  public let kSAFirstElement: Int = 0
  public let kSANameMainDirectory = "imagenes"
  public let kSANameFilesDirectory = "Archivos"

  // MARK: - Llaves para los servicios
  public let KSAHTTPMethodGet: String = "GET"
  public let kSAHTTPMethodPOST: String = "POST"
  public let kSAExecutive: String = "voEjecutivo"
  public let kSAPeriodoSales: String = "dtoResponse"
  public let kSADATA: String = "data"
  public let kSADataDevPlan: String = "plan_desarrollo"
 

  // MARK: - Llaves para los servicios SIMULADOS
  public let kSASimulationResponse: String = "SimulationResponse"
  public let kSALoginResponse: String = "Login_Response"
  public let kSALogoutResponse: String = "Logout_Response"
  public let kSAPeriodSalesResponse: String = "PeriodSales_Response"
  public let kSAAnnualReportResponse:String = "AnnualReport_Response"
  public let kSAExecutiveResponse:String = "Executive_Response"
  public let kSASaveQuestionsResponse:String = "SaveQuestions_Response"
  public let kSAQuestionsResponse:String = "Question_Response"
  public let kSASalesPeriodVision12Response:String = "SalesPeriodVision12_Response"
  public let kSAImageProfileResponse:String = "ImageProfile_Response"
  public let KSADataLinkingResponse:String = "DataLinking_Response"
  public let KSADevelopmentPlanResponse: String = "DevelopmentPlan_Response"

  public let kSASectionZero: Int = 0
  public let kSASectionOne: Int = 1
  public let kSATwoSections: Int = 2

   // MARK: - XIB NAME
  public let kSACloseSessionXIBName: String  = "SACloseSessionView"
  public let kSASearchNameXIBName: String  = "SearchNameTableViewCell"
  public let kSASearchExecutiveTableViewCellXIBName: String  = "SASearchExecutiveTableViewCell"
  public let kSAAccompanimentXIBName: String  = "SearchNameAccompanimentView"
  public let kSACarViewAmountXIBName: String = "SACardViewAmount"
  public let kSACarViewPercentageXIBName: String = "SACardViewPercentage"
  public let kSASession1to1XIBName: String = "Session1to1"
  public let kSASession1to1IdentifiercVC: String = "SASession1to1TracingViewControllerId"

  // MARK: - Acompañamiento
  public let kSAAccompanimentStoryboard: String = "Accompaniment"
  public let kSAEmbedSegueProfile: String = "EmbedSegueProfileAccompaniment"
  public let kSANameViewAccompaniment: String  = "View_Accompaniment"
  public let kSASearchNameCell: String = "SearchNameCell"
  public let kSAPopoverViewController: String = "SADataTableViewController"
  public let kSAPopoverInformationVC: String = "SAInformationViewController"

  public let kSAOptionTypeVisit:Int = 1
  public let kSAOptionTypeAccompaniment:Int = 2
  public let kSAPageController: String = "PageController"
  public let kSATitle: String = "title"
  public let kSAType: String = "type"
  public let kSATypeRange: String = "Por escala"
  public let kSATypeTrueFalse: String = "si/no"
  public let kSATypeObservations: String = "OBSERVATIONS"

  public let kSAIdentifierVCQuestionRange: String = "QuestionRangeVC"
  public let kSAIdentifierVCQuestionTrueFalse: String = "QuestionTrueFalseVC"
  public let kSAIdentifierVCObservations: String = "ObservationsVC"
  public let kSAIdentifierVCResults: String = "ResultsVC"

  // MARK: - Líder
  public let kSAEmbedSegueIndicatorsLeader: String = "EmbedSegueIndicatorsLeader"
  public let kSAEmbedSegueCombinedChartsLeader: String = "EmbedSegueCombinedChartsLeader"
  public let kSAEmbedSeguePercentageLeader: String = "EmbedSeguePercentageLeader"

  // MARK: - Date
  public let kSATitleSelectionDate: String = "Title_selection_date"
  public let kSATitleSelect: String = "Title_select"
  public let kSATitleConsult: String = "Title_consult"
  public let kSATitlePeriod: String = "Title_period"
  public let kSADateSelectorStoryboard: String = "DateSelector"
  public let kSACalendarySelectorStoryboard: String = "CalendaryVC"
  public let kSAIndicatorsViewSegue: String = "showIndicatorsView"
  public let kSAIndicatorsPadViewSegue: String = "showIndicatorsiPad"


  //MARK: - Menu lateral
  public let kSAFileMenuLateralItemsWithCalendary: String = "MenuItemsWithCalendary"
  public let kSAFileMenuLateralItemsWithoutCalendary: String = "MenuItemsWithoutCalendary"

  public let kSAFileMenuPhoneItemsDashboard: String = "MenuItemsPhoneDashboard"
  public let kSAFileMenuPhoneItemsVision: String = "MenuItemsVision"
  public let kSAFileMenuPhoneItemsWithCalendary: String = "MenuItemsPhoneWithCalendary"

  // MARK: - Perfil
  public let kSACloseSessionVC: String = "CloseSessionVC"
  public let kSAProfileStoryboard: String = "Profile"

  // MARK: - Email
  public let kSAEmailStoryboard: String = "Email"
  public let kSAIdentifierVCEmailSend: String = "EmailSendVC"
  public let kSAIdentifierVCViewEmail: String = "ViewEmailVC"
  public let kSAEmailDefaultSender: String = "suramexico@suramexico.com"
  public let kSAEmailWrongRecipient: String = "Message_wrong_email"
  public let kSAEmailMissingRecipient: String = "Message_recipient_missing"
  public let kSAEmailPalceholderRecipient: String = "Placeholder_recipient_field"
  public let kSAIdentifierVCTokenView: String = "TokenView"
  //MARK: - CheckList

  public let kSACheckListStoryboard: String = "CheckList"
  public let kSAIdentifierVCResultsCheckList: String = "ResultsCheckListVC"
  public let SACellCheckList: String = "CellQuestionCheckList"
  public let kSAMessageAlertNOTQuestionsCheckList: String = "Title_not_questions_checklist"
  public let kSAMessageConsultingQuestions: String = "Title_Consulting_Questions"


  //MARK: - Session 1 a 1
  public let kSAPlaceHolder_Session1a1_YouCompromise: String = "Title_PlaceHolder_Session1a1_YouCompromise"
  public let kSAPlaceHolder_Session1a1_ICompromise: String = "Title_PlaceHolder_Session1a1_ICompromise"
  public let kSAPlaceHolder_Session1a1_Observations: String = "Title_PlaceHolder_Session1a1_Observations"
  public let kSAStringTitle_Session1a1: String = "Title_session1a1"

  //MARK: - Service Survey
  public let kSAStringTitle_ServiceSurvey: String = "Title_serviceSurvey"
  public let kSAServiceSurveyStoryboard: String = "ServiceSurvey"
  public let SACellServiceSurvey: String = "CellQuestionServiceSurvey"
  public let kSAPageControllerServiceSurvey: String = "PageControllerServiceSurvey"
  public let kSAIdentifierVCQuestionRangeServiceSurvey: String = "QuestionRangeServiceSurveyVC"
  public let kSAIdentifierVCQuestionTrueFalseServiceSurvey: String = "QuestionTrueFalseServiceSurveyVC"
  public let kSAIdentifierVCResultsServiceSurvey: String = "ResultsServiceSurveyVC"
  public let kSAMessageAlertNOTQuestionsServiceSurvey: String = "Title_not_questions_serviceSurvey"
  public let kSAIdentifierVCObservationsServiceSurvey: String = "ObservationsServiceSurveyVC"

  public let kSAStringTitle_Session1a1TitleComments: String = "labelTitleComments"
  public let kSAStringTitle_Session1a1TitleAgreements: String = "labelTitleAgreements"
  public let kSAStringTitle_Session1a1TitleEmployeeAgreements: String = "labelTitleEmployeeAgreements"

  public let kSAStringTitle_BlackBoard: String = "Title_BlackBoard"
  public let kSATitleButtonFinishEdition: String = "Title_button_finishedition"
  public let kSATitleButtonReturnGalery: String = "Title_button_return_galery"
  public let kSATitleButtonReturnTracing: String = "Title_button_return_tracing"
  public let kSAAccept:String = "Title_Accept"

  //MARK: - Plan de Desarrollo
  public let kSAPopoverDevPlanVC: String = "SARecordViewController"
  public let kSAPopoverSegue: String = "popoverSegue"
  public let kSAMessageConsultInformation: String = "Message_Consulting_Information"
  public let SATitleDevelopmentPlan = "Title_Development_Plan"
  public let kSAStringTitle_DevelopmentPlan = "Title_Development_Plan"
  public let kSADevelopmentPlanStoryboard: String = "DevelopmentPlan"

  //MARK: - Datos con Enlace
  public let SATitleDataLinking = "Title_Data_Linking"
  public let SAStatusLabel = "labelStatus"
  public let SAExpiredDateLabel = "labelExpiredDate"
  public let SACertifiedLabel = "labelCertified"
  public let SAAuthorizedLabel = "labelAuthorized"
  public let SAAttorneyLabel = "labelAttorney"
  public let SAApprovedLabel = "labelApproved"
  public let SAWithTradeLabel = "labelWithTrade"
  public let SAExtraordinaryLabel = "labelExtraordinary"
  public let SAFailedLabel = "labelFailed"
  public let SANoApplyLabel = "labelNoApply"
  public let kSAPopoverHelpSegue: String = "popoverHelpSegue"

  // MARK: - Juntas grupales
  public let kSATitle_GroupMeeting = "Title_Group_Meeting"
  public let kSATitleFileSaved = "Title_Files_Saved"
  public let kSAFolderContentSegue: String = "showFolderContentSegue"
  public let kGroupMeetingCell = "groupMeetingCell"
  public let kSAGroupMeetingCellNib = "GroupMeetingsCollectionViewCell"
  public let kSAMessageLoading = "Message_Loading"
  public let kSAShowFilesSegue: String  = "showFilesSegue"
  public let kSAMessageDownloading = "Message_Downloading"
  public let KSAImageMoreIcon = "more-icon"
  public let kSAExtensionZIP = "zip"
  public let kSAExtensionMP4 = "mp4"
  public let KSAExtensionPDF = "pdf"
  public let kSAExtension = "."
  public let kSAMessageAdvertisementNetwork = "Message_advertisement_Network"

  //MARK: Profile
  public let SAStoryboardProfilePhone: String = "ProfilePhone"
  public let SAViewControllerProfilePhone: String =  "ProfilePhoneVC"

  //MARK: Login
  public let kSAUserTextfieldPlaceholder: String = "Placeholder_username"
  public let kSAPasswordTextfieldPlaceholder: String =  "Placeholder_password"

  //MARK:- Indicadores Phone
  public let kSASectionOfIndicatorsVC:String = "SASectionOfIndicatorsViewController"
  public let kSelectorVision12months:String = "showVision12months"
  public let SAStoryboardVision: String =  "VisionTwelveMonths"
  public let SAStoryboardIndicatorsiPhoneIdentifier: String =  "SAIndicatorsPhoneViewControllerId"
  public let SAStoryboardIndicatorsGenericName: String =  "IndicatorsGeneric"
  public let SAStoryboardIndicatorsiPadIdentifier: String =  "SAIndicatorsPadViewControllerId"

  //MARK:- observaciones

  public let kSAObservations: String = "observaciones"
  public let kSATypeModeling: String = "Modelaje"
  public let kSATypeMultiproduct: String = "Multiproducto"

  //MARK:- Vision 12 meses iPhone
  public let kSAVision12Cell: String = "Vision12Cell"
  public let kSAVision12CellNib: String = "SAVision12TableViewCell"

  //MARK:- Generales
  public let kSAIdentifierVCGeneral: String = "DependentsId"
  public let kSAGeneralVCiPhoneName: String = "Dependents"
  public let kSAMessageEmptyList: String = "Message_Generales_Empty_List"
  public let kSASuccesRequest:String = "S"
  public let SASegueSendEmailID: String = "showEmailId"
  public let SASegueSession1to1ID: String = "SASession1to1Id"
  public let SAGeneralTableViewCellId: String = "GeneralTableViewCellId"
  public let SAIndentifierGenericIntroductionVC: String = "GenericIntroductionVC"

  //MARK:- Nombre de clases
  public let SANameClassCellSASession1to1Tracing: String = "SASession1to1TracingTableViewCellId"
  public let SANameClassVCSASession1to1Tracing: String = "SASession1to1TracingViewController"
  public let SANameClassGaleryViewController: String =  "SAGaleryViewController"
  public let SANameClassPreviewImageViewController: String = "SAPreviewImageViewController"
  public let SANameClassSession1to1GaleryVC: String = "SASession1to1GaleryViewController"
  public let SANameClassBlackboardVC: String = "SABlackboardViewController"
  public let SANameClassCalendaryVC: String = "SACalendaryViewController"
  public let SANameClassCalendaryAnnualVC: String = "SAAnnualCalendaryViewController"
  public let SANameClassLoginVC: String = "SALoginViewController"
  public let SANameClassLateralMenuVC: String = "SALateralMenuViewController"
  public let SANameClassProfileVC: String = "SAProfileViewController"
  public let SANameClassChecklistVC: String = "SACheckListViewController"
  public let SANameClassServiceSurveyVC: String = "SAServiceSurveyViewController"
  public let SANameClassGenericIntroductionVC: String = "SAGenericIntroductionViewController"
  public let SANameClassSession1to1VC: String = "SASession1to1ViewController"
  public let SANameClassSearchExecutiveVC: String = "SASearchExecutiveViewController"
  public let SANameClassCheckListQuestionsVC: String = "SACheckListQuestionsViewController"
  public let SANameClassCheckListResultsVC: String = "SACheckListResultsViewController"
  public let SANameClassEmailVC: String = "SAEmailViewController"
  public let SANameClassEmailSendVC: String = "SAEmailSendViewController"
  public let SANameClassDashboardVC: String = "SADashboardViewController"
  public let SANameClassDependentsVC: String = "SADependentsViewController"
  public let SANameClassVision12MonthsVC: String = "SAVision12MonthsViewController"
  public let SANameClassIndicatorsVC: String = "SAIndicatorsViewController"
  public let SANameClassAccompanimentResultsVC: String = "SAResultsAccompanimentViewController"
  public let SANameClassObservationsVC: String = "SAObservationsViewController"
  public let SANameClassPageVC: String = "SAPageViewController"
  public let SANameClassAccompanimentVC: String = "SAAccompanimentViewController"
  public let SANameClassGaleryAllImagesVC: String = "SAGaleryAllImagesViewController"

  //MARK: - Lista de pantallas registradas en google

  public let SAAnalitycsProfile: String = "Perfil"
  public let SAAnalitycsDashboardVC: String = "Dashboard"
  public let SAAnalitycsLoginVC: String = "Login"
  public let SAAnalitycsIntroductionSession1a1: String = "Sesión 1 a 1 - Introducción"
  public let SAAnalitycsSession1a1SearchVC: String = "Sesión 1 a 1 - Búsqueda ejecutivos"
  public let SAAnalitycsSession1a1: String = "Sesión 1 a 1 - Inicio"
  public let SAAnalitycsSession1to1Galery: String = "Sesión 1 a 1 - Galería"
  public let SAAnalitycsSession1to1PreviewImage: String = "Sesión 1 a 1 - Previsualización de Imagen"

  public let SAAnalitycsGalery: String = "Galería"
  public let SAAnalitycsGaleryAllImages: String = "Galería - Todas las imágenes"
  public let SAAnalitycsPreviewImage: String = "Galería - Previsualización de Imagen"

  public let SAAnalitycsSession1to1EmailVC: String = "Sesión 1 a 1 - "
  public let SAAnalitycseChecklistEmailVC: String = "Checklist - "
  public let SAAnalitycsAccompanimentEmailVC: String = "Acompañamiento - "
  public let SAAnalitycsBlackboardEmailVC: String = "Pizarrón - "
  public let SAAnalitycsGaleryEmailVC: String = "Galería - "
  public let SAAnalitycsTracingEmailVC: String = "Seguimiento - "

  public let SAAnalitycsOKEmailVC: String = "Envío de Email"
  public let SAAnalitycsErroEmailVC: String = "Error en envío de Email"
  public let SAAnalitycsEmailVC: String = "Email"

  public let SAAnalitycsAccompanimentVC: String = "Acompañamiento - Inicio"
  public let SAAnalitycsAccompanimentQuestionaryVC: String = "Acompañamiento-Cuestionario"
  public let SAAnalitycsAccompanimentResultsVC: String = "Acompañamiento - Resultados"
  public let SAAnalitycsIntroductionAccompaniment: String = "Acompañamiento - Introducción"
  public let SAAnalitycsAccompanimentSearchVC: String = "Acompañamiento - Búsqueda ejecutivos"
  public let SAAnalitycsAccompanimentObservationsVC: String = "Acompañamiento - Observaciones"

  public let SAAnalitycsChecklistResultsVC: String = "CheckList - Resultados"
  public let SAAnalitycsChecklistSearchVC: String = "Checklist - Búsqueda ejecutivos"
  public let SAAnalitycsIntroductionChecklist: String = "Checklist - Introducción"
  public let SAAnalitycseChecklistQuestionary: String = "CheckList - Cuestionario"
  public let SAAnalitycsChecklistVC: String = "Checklist - Inicio"

  public let SAAnalitycsCalendaryAnnualVC: String = "Calendario Anual"
  public let SAAnalitycsCalendaryVC: String = "Calendario"
  public let SAAnalitycsBlackboard: String = "Pizarrón"
  public let SAAnalitycsTracing: String = "Seguimientos"
  public let SAAnalitycsDependentsVC: String = "Dependientes"
  public let SAAnalitycsHistoricalVC: String = "Visión 12 Meses"
  public let SAAnalitycsIndicatorsVC: String = "Indicadores"
  public let SAAnalitycsMenuVC: String = "Menu Lateral"

  public let SAAnalitycsServiceSurveyEmailVC: String = "Encuesta de servicio - "
  public let SAAnalitycsIntroductionServiceSurvey: String = "Encuesta de servicio - Introducción"
  public let SAAnalitycsServiceSurveyObservationsVC: String = "Encuesta de servicio - Observaciones"
  public let SAAnalitycsServiceSurveySearchVC: String = "Encuesta de servicio - Búsqueda ejecutivos"
  public let SAAnalitycsServiceSurveyVC: String = "Encuesta de servicio"

  public let SAAnalitycsDevelopmentProgramSearchVC: String = "Plan de desarrollo - Búsqueda ejecutivos"
  public let SAAnalitycsIntroductionDevelopmentPlan: String = "Plan de desarrollo - Introducción"

  //Categorias
  public let kSAAccompanimentCategory: String = "Acompañamiento"
  public let kSAChecklistCategory: String = "Check List"
  public let kSASession1to1Category: String = "Sesión 1&1"
  public let kSAServiceSurveyCategory: String = "Encuesta de Servicio"
  public let kSADevelopmentPlanCategory: String = "Plan de desarrollo"

//Flow
//Para fines de tracking se le habilita la dimensión personalizada "Flujo "identificando un flujo
  public let kSAAccompanimentFlow: String = "accomp"
  public let kSAChecklistFlow: String = "checklist"
  public let kSASession1to1Flow: String = "sesion"
  public let kSAServiceSurveyFlow: String = "encuesta"

  public let kSADevelopmentPlanFlow: String = "Plan de desarrollo"
  public let kSALoginInFlow: String = "Inicio de sesión"
  public let kSABlackboardFlow: String = "Pizarrón"
  public let kSAGaleryFlow: String = "Galería"
  public let kSATracingFlow: String = "Seguimiento"

  public let INDEX_USERID:UInt = 1
  public let INDEX_PERFILID:UInt = 2
  public let INDEX_GERENCIA:UInt = 3
  public let INDEX_PUESTO:UInt = 4
  public let INDEX_FIGURA:UInt = 5
  public let INDEX_ESTRUCTURA:UInt = 6
  public let INDEX_EMAIL_DESTINATARIO:UInt = 7
  public let INDEX_EMAIL_ORIGEN:UInt = 8
  public let INDEX_FLUJO:UInt = 9
  public let INDEX_FLUJO_METRICA:UInt = 1

  //Para fines de tracking de la métrica FlujoRealizado se le dará un valor de tipo entero al flujo al que se le esté dando seguimiento:
  public let FLOW_CHECKLIST:String = "1"
  public let ​FLOW_SESION1TO1:String = "2"
  public let ​FLOW_SURVEY:String = "3"
  public let ​FLOW_ACCOMP:String = "4"


  //Acciones
  public let kSAActionEmailSend_Accompaniment: String = "Envio_email_ok_acompañamiento"
  public let kSAActionEmailSend_Checklist: String = "Envio_email_ok_checklist"
  public let kSAActionEmailSend_Session1to1: String = "Envio_email_ok_sesion1a1"
  public let kSAActionEmailSend_SeviceSurvey: String = "Envio_email_ok_encuesta"

  //Etiquetas
  public let kSAActionEmailSend_Accompaniment_Label: String = "Termino_acompañamiento_recibio_codigo"
  public let kSAActionEmailSend_Checklist_Label: String = "Termino_checklist_recibio_codigo"
  public let kSAActionEmailSend_Session1to1_Label: String = "Termino_sesion1a1_recibio_codigo"
  public let kSAActionEmailSend_SeviceSurvey_Label: String = "Termino_encuesta_recibio_codigo"


  // MARK: - Llave de strings
  public let kSAStringTitleAccompaniment: String = "Title_accompaniment"
  public let kSAStringTitleButtonStartAccompaniment: String = "Title_Start_Accompaniment"
  public let kSAStringTitlePersonEvaluator: String = "Title_Person_Evaluator"
  public let kSAStringTitleQuestion: String = "Title_Question"
  public let kSAStringTitleButtonNewAccompaniment: String = "Title_new_accompaniment"
  public let kSAStringTitleButtonHistoricalAccompaniment: String = "Title_historical_accompaniment"
  public let kSAMessageAlertNOTQuestionsForAccompaniment: String = "Title_not_questions_accompaniment"
  public let kSAStringTitlePersonExecutive: String = "Title_Person_Executive"
  public let kSAStringTitleProgramExecutive: String = "Title_Program_Executive"
  public let kSAStringTitleTypeVisit: String = "Title_Type_Visit"
  public let kSAStringTitleTypeAccompaniment: String = "Title_Type_Accompaniment"
  public let kSAStringTitleCheckList: String = "Title_CheckList"
  public let kSAStringTitleButtonStartCheckList: String = "Title_button_start_checklist"
  public let kSAStringTitleButtonStartServiceSurvey: String = "Title_button_start_serviceSurvey"
  public let kSAStringMessageCannotCreateURL: String = "Message_cannot_create_url"
  public let kSAStringMessageCannotGetIP: String = "Message_cannot_get_ip"
  public let kSAStringMessageCannotGetToken: String = "Message_cannot_get_token"
  public let kSAStringErro: String = "Title_Erro"
  public let kSAStringMessageCannotCreateJson: String = "Message_cannot_create_json"
  public let kSAStringMessageCannotReceiveData: String = "Message_cannot_receive_data"
  public let kSAStringMessageErroGeneric: String = "Message_erro_generic"
  public let kSAStringMessageCannotParsing: String = "Message_cannot_parsing"
  public let kSAStringMessageCannotGetJson: String = "Message_cannot_get_json"
  public let kSAStringMessageNotImages: String = "Message_not_images"
  public let kSAStringMessageNotImagesInGalery: String = "Message_not_images_in_galery"
  public let kSAStringMessageDeleteAlbum: String = "Message_delete_album"
  public let kSAStringMessageOK: String = "Message_OK"
  public let kSAStringMessageAlert: String = "Message_alert"
  public let kSAStringTitleAddSession: String = "Title_Add_Session"
  public let kSAStringComment: String = "Commet_title"
  public let kSATitleViewGaleryAlbum: String = "Title_view_galery_albums"
  public let kSATitleViewGalery: String = "Title_view_galery"
  public let kSATitleAlbum: String = "Title_Album"
  public let kSATitleCurrentUpdate: String = "Title_Current_Update"
  public let kSATitleAllAlbums: String = "Title_All_Albums"
  public let kSATitleShowImages: String = "Title_Show_Images"
  public let kSATitleAddCurrentImages: String = "Title_add_images_current"
  public let kSATitleAllImages: String = "Title_all_images"
  public let kSATitleImagesSelected: String = "Title_images_selected"
  public let kSATitleOneImageSelected: String = "Title_One_image_selected"
  public let kSATitleButtonFinish: String = "Title_Button_Finish"
  public let kSATitleWriteText: String = "Title_Write_text"
  public let kSATitleContinue: String = "Title_Continue"
  public let kSATitleNotice: String = "Title_Notice"
  public let kSATitleAddUser: String = "Title_add_user"
  public let kSATitleAddPassword: String = "Title_add_password"
  public let kSATitleHello: String = "Title_Hello"
  public let kSAMessageConsultPeriod: String = "Message_consult_period"
  public let kSAMessageConsultProfile: String = "Message_updating_profile"
  public let kSAMessageLogin: String = "Message_Login"
  public let kSAMessageAnnualReport: String = "Message_Annual_Report"
  public let kSAMessageScreeshot: String = "Message_Screenshot"
  public let kSAMessageConsultingListExecutive: String = "Message_ConsultingListExecutive"
  public let kSAMessageConsultingSaveQuestions: String = "Message_Consulting_Save_Questions"
  public let kSAMessageSaveQuestions: String = "Message_Save_Questions"
  public let kSAMessageInstructionsCheckList: String = "Message_Instructions_Checklist"
  public let kSAMessageInstructionsServiceSurvey: String = "Message_Instructions_ServiceSurvey"
  public let kSAMessagePlaceholderAccompaniment: String = "Placeholder_Accompaniment"
  public let kSAMessageDataLinking: String = "Message_Data_Linking"
  public let kSAMessageDevelopmentPlan: String = "Message_Development_Plan"
  public let kSAResponseTrue: String = "Response_True"
  public let kSAResponseFalse: String = "Response_False"
  public let kSAResponseRangeOne: String = "Response_RangeOne"
  public let kSAResponseRangeTwo: String = "Response_RangeTwo"
  public let kSAResponseRangeThree: String = "Response_RangeThree"
  public let kSAResponseRangeFour: String = "Response_RangeFour"
  public let kSAResponseRangeFive: String = "Response_RangeFive"
  public let kSAResponseRangeOneServiceSurvey: String = "Response_RangeOneServiceSurvey"
  public let kSAResponseRangeTwoServiceSurvey: String = "Response_RangeTwoServiceSurvey"
  public let kSAResponseRangeThreeServiceSurvey: String = "Response_RangeThreeServiceSurvey"
  public let kSAResponseRangeFourServiceSurvey: String = "Response_RangeFourServiceSurvey"
  public let kSAResponseRangeFiveServiceSurvey: String = "Response_RangeFiveServiceSurvey"

  public let kSAStringTitleObservations : String = "Title_Observations"
  public let kSATitleShowResults: String = "Title_ShowResults"
  public let kSAInformationToShowInQuestion: String = "Information_Question"
  public let kSAMessageAlertSelectResponse: String = "Message_alert_select_response"
  public let kSAMessageAlertTypeVisit: String = "Message_alert_type_visit"
  public let kSAMessageAlertTypeAccomp: String = "Message_alert_type_accomp"
  public let kSATitleResultByEmail: String = "Title_button_send_result_by_email"
  public let kSATitleScore: String = "Title_Score"
  public let kSATitlePercentage: String = "Title_Percentage"
  public let kSATitleSemaphore: String = "Title_Semaphore"
  public let kSATitleQualification:String = "Title_Qualification"
  public let kSATitleButtonReturnAccomp: String = "Title_button_return_accomp"
  public let kSATitleButtonReturnServiceSurvey: String = "Title_button_return_serviceSurvey"
  public let kSATitleButtonBackCheckList: String = "Title_button_return_check_list"
  public let kSATitleButtonReturnSession1a1: String = "Title_button_return_session1a1"
  public let kSATitleResults: String = "Title_results"
  public let kSATitleSendEmail: String = "Title_email_send"
  public let kSATitleErrorSendEmail: String = "Title_Error_send_email"
  public let kSATitleResultsTypeModeling: String = "Title_results_typeModeling"
  public let kSANotExecutiveToShow: String = "Message_NOT_Executive"
  public let kSAMessageCloseSession: String = "Message_close_session"
  public let kSAMessageNotConection: String = "Message_not_conection_internet"
  public let kSAStringTitleSession: String = "Title_Session"
  public let kSAStringTitleView: String = "Title_View"
  public let kSANameViewProfile: String = "View_Profile"
  public let kSANotDataGrafics:String = "Not_data_grafics"
  public let kSATitleIndicators:String = "Title_Indicators"
  public let kSATitleIndicatorsSMART:String = "Title_IndicatorsSMART"
  public let kSATitleMultiproduct:String = "Title_Multiproduct"
  public let kSATitleModifierPay:String = "Title_ModifierPay"
  public let kSATitleTeamIntegrality:String = "Title_TeamIntegrality"
  public let kSATitleLowRotation:String = "Title_LowRotation"
  public let kSATitleLows:String = "Title_Lows"
  public let kSATitleActual:String = "Title_Actual"
  public let kSATitleIntegralTemplate:String = "Title_IntegralTemplate"
  public let kSATitleHeadCount:String = "Title_HeadCount"
  public let kSATitleProductivity:String = "Title_DetailProductivity"
  public let kSATitleMissing:String = "Title_Missing_DetailProductivity"
  public let kSATitleGoal = "Title_Goal"
  public let kSATitleDetailRCV :String = "Title_Detail_RCV"
  public let kSATitleDetailNETEO :String = "Title_Detail_NETEO"
  public let kSATitleDetailSAVE :String = "Title_Detail_SAVE"
  public let kSATitleDetailInsurance :String = "Title_Detail_INSURANCE"
  public let kSATitleRCV:String = "Title_RCV"
  public let kSATitleNETEO:String = "Title_NETEO"
  public let kSATitleSAVE:String = "Title_SAVE"
  public let kSATitleInsurance:String = "Title_INSURANCE"
  public let kSATitleGotoVision12Month:String = "Title_GOTOVision12Month"
  public let kSATitleAverageGeneral:String = "TitleAverageGeneral"
  public let kSATitleFARMER:String = "TitleFARMER"
  public let kSATitleHUNTER:String = "TitleHUNTER"
  public let kSATitleAverageGeneralProductivity:String = "TitleProductivityAverageGeneral"
  public let kSAMessageHistoricalReport: String = "Message_Historical_Report"
  public let kSAMessageLoginErrorUser: String = "Message_Login_Error_User"
  public let kSAMessageSaveImage: String = "Message_SaveImage"


  public let kSAIndentifierPageHistorical :String = "PageItemVision12"
  public let kSAIndentifierCalendaryCollectionView :String = "cellCollectionView"
  public let kSATitleProductivityMultiproduct:String = "Title_Productivity_Multiproduct"
  public let kSATitleIntegralityTeam:String = "Title_Integrality_Team"
  public let kSATitlePlanInduction : String = "Title_Plan_Induction"
  public let kSATitlePlanAlert :String = "Title_Plan_Alert"
  public let kSATitlePlanCore :String = "Title_Plan_Core"
  public let kSATitlePlanMultiproduct :String = "Title_Plan_Multiproduct"
  public let kSATitlePlanMaintenance :String = "Title_Plan_Maintenance"
  public let kSATitlePlanFaststart :String = "Title_Plan_Fast_start"
  public let kSATitleForGraficIntegralityTeam:String = "Title_For_Grafic_Integrality_Team"
  public let kSATitleForGraficProductivityAverage :String = "Title_For_Grafic_Productivity_Average"
  public let kSAMessageDeleteImage:String = "Message_Delete_Image"
  public let kSAOptionAlertDelete:String = "Message_Delete_Option"
  public let kSAOptionDeletePhoto:String = "Message_Delete_Photo"
  public let kSAMessageWithoutResults:String = "Message_Without_Results"
  public let kSAMessageRegistryDelete:String = "RegistryDelete"
  public let kSATitleSend :String = "TitleSend"
  public let kSATitleTracing :String = "Title_Tracing"
  public let kSATitleCancel:String = "Title_Cancel"
  public let kSATitleCamera:String = "Title_Camera"
  public let kSAMessageNoCamera: String = "Message_No_Camera"
  public let kSAMessagePhotoSaved: String = "Message_Photo_Saved"
  public let kSAMessagePhotoDeleted: String = "Message_Photo_Deleted"
  public let kSATitleAllPhotos:String = "Title_AllPhotos"
  public let kSATitleNext:String = "Title_Next"
  public let kSATitleAdd:String = "Title_Add"
  public let kSAMessageExceededLimitImages:String = "Message_Exceeded_Limit_Images"
  public let kSAMessageDeleteImagesInGalery:String = "Message_Delete_Images_In_Galery"
  public let kSAMessageNotSelectedImagesInGalery:String = "Message_Not_Selected_Images_In_Galery"
  public let SATitleCalendary: String = "Title_Calendary"
  public let SATitleGoOut: String = "Title_GoOut"
  public let SATitleCloseSession: String = "Title_CloseSession"
  public let SAMessageHasSessionsSaved: String = "Message_HasSessionsSavedYouWantCloseSession"
  public let SAMessageChecklist: String = "Message_Checklist"
  public let SAMessageServiceSurvey: String = "Message_ServiceSurvey"
  public let SAMessageAccompaniment: String = "Message_Accompaniment"
  public let SAMessageSession1to1: String = "Message_Session1to1"
  public let SAMessageDevelopmentPlan: String = "Message_DevelopmentPlan"
  public let SATitleChecklist: String = "Title_Checklist"
  public let SATitleServiceSurvey: String = "Title_serviceSurvey"
  public let SATitleStartChecklist: String = "Title_Start_Checklist"
  public let SATitleStartServiceSurvey: String = "Title_Start_ServiceSurvey"
  public let SATittleGroupMeeting:String = "Tittle_GroupMeeting"
  public let SATitlePersonalGoal: String = "Title_PersonalGoal"
  public let SATitleStartSession1a1: String = "Title_Start_Session1a1"
  public let SAMessageSaveStartSession: String = "Message_SaveStartSession"
  public let SATitleSave: String = "Title_Save"
  public let SAMessageGoOutWithoutSaved: String = "Message_GoOutWithoutSaved"
  public let SAMessageUpdate: String =  "Message_Update"
  public let SATitleUpdate: String = "Title_Update"
  public let SAMessageGoOutWithoutUpdate: String =  "Message_GoOutWithoutUpdate"
  public let SATitleViewSearchInDevelopmentPlan: String =  "TitleViewSearchInDevelopmentPlan"
  public let SATitleViewSearchRecentlies: String = "TitleViewSearchRecentlies"
  public let SATitleViewSearchExecutives: String = "TitleViewSearchExecutives"
  public let SATitleComments: String = "Title_Comments"
  public let SATitleLeadingAgreement: String = "Title_LeadingAgreements"
  public let SATitleCollaborativeAgreements: String = "Title_CollaborativeAgreements"
  public let SATitleSendEmail: String = "Title_SendEmail"
  public let SAMessageErroToSendEmail: String = "Message_ErroToSendEmail"
  public let SAMessageInConstruction: String = "Message_InConstruction"
  public let SATitleDependents: String = "Title_Dependents"
  public let SAMessageNotDataToShow: String = "Message_Not_data_To_Show"
  public let SAMessageWithoutDependents: String = "Message_WithoutDependents"
  public let SATitleStartAccompaniment: String = "Title_Start_Accompaniment"
  public let SAMessageAccompanimentsInstructions1: String = "MessageAccompanimentsInstructions1"
  public let SAMessageAccompanimentsInstructions2: String = "MessageAccompanimentsInstructions2"
  public let SAMessageAccompanimentsInstructions3: String = "MessageAccompanimentsInstructions3"
  public let SAMessageAccompanimentsInstructions4: String = "MessageAccompanimentsInstructions4"

  public let SAMessageNotTracing: String = "MessageNotTracing"
  public let SASelectAMonth:String = "Title_Select_Month"
  public let SAMessageGoToPointsClubSite: String = "Message_GoToPointsClubSite"
  public let SAMessageGoToCommissionsSite: String = "Message_GoToCommissionsSite"
  public let SAMessageGoToFigureReferenceSite: String = "Message_GoToFigureReferenceSite"
  public let SAMessagePointsClubURL: String = "Message_PointsClub_URL"
  public let SATitleMultiproduct = "Title_Multiproduct"
  public let SATitleCore = "Title_Core"
  public let SATitleMaintenance = "Title_Maintenance"
  public let SATitleGoal = "Title_Goal"
  public let SATitleReal = "Title_Real"
  public let SAMessagePersonalGoalNoGoalsSaved: String = "MessageNoGoalsSaved"
  public let SAMessagePersonalGoalClosedGoal: String = "MessageClosedGoals"
  public let SAMessagePersonalGoalAtLeastDescription: String = "MessageAtLeastDescription"
  public let SATitleEdit = "Modificar"
  public let SAMessageSavedPhotoProfile: String = "MessageSavedSuccesfully"
  public let SAMessageDeletedPhotoProfile: String = "MessageDeletedSuccesfully"
  public let SAMessageSaveDataTool: String = "MessageSaveDataTool"
  public let SAMessageNotSaveDataTool: String = "MessageNotSaveDataTool"
  public let SAMessageHelpPersonalGoal: String = "MessageHelpPersonalGoal"
  public let SAMessageGoToSuraWordl: String = "Message_GoToSuraWorldSite"
