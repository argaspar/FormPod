//
//  SASlateFormViewController
//  FormPod
//
//  Created by Rafael Gaspar on 09/05/18.
//  Copyright © 2018 NutSystem. All rights reserved.
//

import UIKit
import Eureka

class SASlateFormViewController: FormViewController {
    
    // Struct for form items tag constants
    struct FormItems {
        static let name = "nombreCliente"
        static let date = "fecha"
        static let day = "dia"
        static let rcv = "rcv"
        static let av = "av"
        static let fondos = "fondos"
        static let seguros = "seguros"
        static let salidas = "salidas"
        static let montoRCV = "montoRCV"
        static let montoAV = "montoAV"
        static let montoSeguros = "montoSeguros"
        static let montoFondos = "montoFondos"
        static let montoSalidas = "montoSalidas"
        static let ayuda = "ayuda"
        static let ofertaSura = "ofertaSura"
        static let comentarios = "comentarios"
    }
    
    var selectedDayName: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundView = UIImageView.init(image: UIImage (named: "blurEffect"))
        createForm()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// Inicia la construccion del formulario
    internal func createForm(){
        LabelRow.defaultCellUpdate = { cell, row in
            cell.contentView.backgroundColor = .red
            cell.textLabel?.textColor = .white
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 13)
            cell.textLabel?.textAlignment = .right
            
        }
        IntRow.defaultCellUpdate = { cell, row in
            cell.height = { 50 } // increase the height 6 pt
            cell.contentView.layoutMargins.left = 20 // set up left margin to 20
            cell.contentView.backgroundColor = .clear

        }
        /// Seccion cliente
        form +++ Section("Cliente")
            <<< TextRow(FormItems.name) { row in
                row.title = "Nombre"
                row.placeholder = "Escribe el nombre aquí"
                }.cellUpdate{ cell,row in
                    //cell.backgroundColor = .clear
                }
        /// Seccion Fecha
        form +++ Section("Fecha")
            <<< DateRow(FormItems.date) { row in
                row.title = "Fecha"
                row.value = Date()
                selectedDayName = self.getDayofWeek(currentDate: row.value!)
                }.onChange{ dateRow in
                    let currentDateString = self.getDayofWeek(currentDate: dateRow.value!)
                    print(currentDateString)
                    self.selectedDayName = currentDateString
                    self.form.rowBy(tag: FormItems.day)?.updateCell()
            }
            <<< TextRow(FormItems.day) { row in
                row.title = "Día"
                row.tag = FormItems.day
                row.value = self.selectedDayName
                row.disabled = true
                }.cellUpdate{ cell, row in
                    cell.textField.text = self.selectedDayName
            }
        /// Seccion productos
        form +++ Section (header: "Productos", footer: "El monto máximo es de $99,999,999")
            <<< PickerInputRow<String>(FormItems.rcv) {row in
                row.title = "RCV"
                row.options = ["Selecciona tipo de RCV","SB0","SB1","SB2","SB3","SB4"]
                row.value = row.options[0]
                }.cellSetup{ cell,row in
                    cell.imageView?.image = UIImage(named: "plus_image")
                    //cell.backgroundColor = .clear
                }
            <<< IntRow(FormItems.montoRCV){ row in
                row.title = "Monto"
                row.value = 0
                row.titlePercentage = 0.3
                row.add(rule: RuleGreaterThan (min: 0))
                row.add(rule: RuleSmallerThan (max: 100000000))
                let formatter = NumberFormatter()
                formatter.locale = .current
                formatter.numberStyle = .currency
                row.formatter = formatter
                }.cellUpdate(){ cell, row in
                    row.cell.textField.textAlignment = .right
                    row.cell.textLabel?.textAlignment = .right
                    //cell.backgroundColor = .clear
                }
            <<< PickerInputRow<String>(FormItems.av) {row in
                row.title = "AV"
                row.options = ["Selecciona tipo de AV ","AV1","AV2","AV3"]
                row.value = row.options[0]
                }.cellSetup{ cell,row in
                    cell.imageView?.image = UIImage(named: "plus_image")
            }
            <<< IntRow(FormItems.montoAV){ row in
                row.title = "Monto"
                row.value = 0
                row.titlePercentage = 0.3
                let formatter = NumberFormatter()
                formatter.locale = .current
                formatter.numberStyle = .currency
                row.formatter = formatter
                }.cellUpdate(){ cell, row in
                    row.cell.textField.textAlignment = .right
                    row.cell.textLabel?.textAlignment = .right
                }
            <<< PickerInputRow<String>(FormItems.fondos) {row in
                row.title = "Fondos"
                row.options = ["Selecciona tipo de Fondo","FNDE","RETIR","SUR-RV","SUR1","SUR1E","SUR2018","SUR2026","SUR2034","SUR2042","SUR2050","SUR2058","SUR30","SUR30E","SURASIA","SURBN","SURCETE","SURCRP","SURD100","SURGLB","SURGB","SURIPC","SURPAT","SURPLUS","SURREAL","SURUDI","SURUSD","SURV10","SURV20","SURV40","SURVEUR","NAVIGTR","TEMGBIA","FRANPR","FRANUSA","ACTI500","ACTINM","GBMCRE","GBMINF","SUR-DV","SURMILA","Segur Dtal","Segur Libera (inversión)"]
                row.value = row.options[0]
                }.cellSetup{ cell, row in
                    cell.imageView?.image = UIImage(named: "plus_image")
            }
            <<< IntRow(FormItems.montoFondos){ row in
                row.title = "Monto"
                row.value = 0
                row.titlePercentage = 0.3
                let formatter = NumberFormatter()
                formatter.locale = .current
                formatter.numberStyle = .currency
                row.formatter = formatter
                }.cellUpdate(){ cell, row in
                    row.cell.textField.textAlignment = .right
                    row.cell.textLabel?.textAlignment = .right
                }
            <<< PickerInputRow<String>(FormItems.seguros) {row in
                row.title = "Seguros"
                row.options = ["Selecciona tipo de Seguros","Seguro de Vida Practico","Seguro de Vida Protección"]
                row.value = row.options[0]
                }.cellSetup{ cell,row in
                    cell.imageView?.image = UIImage(named: "plus_image")
            }
            <<< IntRow(FormItems.montoSeguros){ row in
                row.title = "Monto"
                row.value = 0
                row.titlePercentage = 0.3
                let formatter = NumberFormatter()
                formatter.locale = .current
                formatter.numberStyle = .currency
                row.formatter = formatter
                }.cellUpdate(){ cell, row in
                    row.cell.textField.textAlignment = .right
                    row.cell.textLabel?.textAlignment = .right
                }
            <<< PickerInputRow<String>(FormItems.salidas) {row in
                row.title = "Salidas"
                row.options = ["Selecciona tipo de Salidas","Compra Casa","Rendimiento","Enfermedad","Viaje","Compro Coche"]
                row.value = row.options[0]
                }.cellSetup{ cell,row in
                    cell.imageView?.image = UIImage(named: "plus_image")
            }
            <<< IntRow(FormItems.montoSalidas){ row in
                row.title = "Monto"
                row.value = 0
                row.titlePercentage = 0.3
                let formatter = NumberFormatter()
                formatter.locale = .current
                formatter.numberStyle = .currency
                row.formatter = formatter
                }.cellUpdate(){ cell, row in
                    row.cell.textField.textAlignment = .right
                    row.cell.textLabel?.textAlignment = .right
                }
        /// Seccion de ayuda
        form +++ Section("Ayuda")
            <<< TextRow(FormItems.ayuda) { row in
                row.title = "Ayuda"
                row.placeholder = "Escribe aquí"
                }.cellUpdate{ cell, row in
                    let button = UIButton(type: .custom)
                    //button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
                    button.frame = CGRect(x: CGFloat(cell.textField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
                    button.addTarget(self, action: #selector(self.showPopoverHelp(_:)), for: .touchUpInside)
                    button.setImage(UIImage(named: "Onboarding"), for: .normal)
                    cell.textField.rightView = button
                    cell.textField.rightViewMode = .always
                    row.cell.textField.textAlignment = .left
                    
            }
            <<< TextRow(FormItems.ofertaSura) { row in
                row.title = "Oferta Sura"
                row.placeholder = "Escribe aquí"
            }.cellUpdate{ cell, row in
                    let button = UIButton(type: .custom)
                    //button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
                    button.frame = CGRect(x: CGFloat(cell.textField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
                button.addTarget(self, action: #selector(self.showPopoverSuraOffer(_:)), for: .touchUpInside)
                    button.setImage(UIImage(named: "Onboarding"), for: .normal)
                    cell.textField.rightView = button
                    cell.textField.rightViewMode = .always
                    row.cell.textField.textAlignment = .left
                    
            }
            <<< TextAreaRow(FormItems.comentarios) { row in
                row.placeholder = "Comentarios"
                row.textAreaHeight = .dynamic(initialTextViewHeight: 50)
            }
        form +++ Section()
            <<< ButtonRow() { row in
                row.title = "Guardar"
                }.onCellSelection(){row ,cell in
                    //let values = self.form.values()
                   
            }
    }
    
    /// Funcion que devuelve el nombre del dia de la semana seleccionado por el Picker
    func getDayofWeek(currentDate: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let currentDateString: String = dateFormatter.string(from: currentDate)
        return currentDateString
    }
    
    /// Funcion que muestra el popover
    func showPopoverViewControllerWithInformation(sender: UIButton, information:String) {
        
        let popController = SAInformationViewController.init(nibName: kSAPopoverInformationVC, bundle: nil)
        popController.information = information
        popController.modalPresentationStyle = UIModalPresentationStyle.popover
        popController.popoverPresentationController?.sourceView = sender
        popController.popoverPresentationController?.sourceRect = sender.bounds
        popController.preferredContentSize = CGSize(width: 280, height: 250)
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.backgroundColor =  UIColor.init(hex: SAColor00226C)
        popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
        
        DispatchQueue.main.async( execute: {
            // Presentar el popover
            self.present(popController, animated: false, completion: nil)
        })
    }
    
    /// Funciones que muestran la informacion al pulsar sobre el boton de ayuda
    @objc func showPopoverHelp(_ sender: UIButton){
        showPopoverViewControllerWithInformation(sender: sender, information: "Pregunta a tu asesor si necesita algún apoyo adicional, tal como un acompañamiento, algún material, capacitación, etc, que le ayude a sentirse mejor preparado para su cita con el cliente y anótalo en este campo")
    }
    
    @objc func showPopoverSuraOffer(_ sender: UIButton){
        showPopoverViewControllerWithInformation(sender: sender, information: "Consulta a tu colaborador las razones de porque eligió esa oferta de productos para el cliente, y evalúen juntos si se hizo un correcto perfilamiento del cliente y si la oferta ofrecida cubre las necesidades financieras del cliente. Agrega aquí si consideras que existe alguna otra solución (producto) que se le debería de mostrar al cliente.")
    }

}

//MARK: - Delegado de UIPopoverPresentationControllerDelegate
extension SASlateFormViewController: UIPopoverPresentationControllerDelegate {
    
    // Para iPad
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    // Para iPhone
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    
}
