//
//  CardView.swift
//  SmartApp
//
//  Created by Karen Sidney on 11/08/17.
//  Copyright © 2017 SURA. All rights reserved.
//
//  Clase que ayuda a agregar bordes a una vista

import UIKit

@IBDesignable

class SACardView: UIView {
  
  /// Radio de la esquina de la vista
  @IBInspectable var cornerRadius: CGFloat = 2
  /// Ancho de la Sombra
  @IBInspectable var shadowOffsetWidth: Int = 0
  /// Altura de la Sombra
  @IBInspectable var shadowOffsetHeight: Int = 1
  /// Color de la Sombra
  @IBInspectable var shadowColor: UIColor? = UIColor.black
  /// Opacidad la Sombra
  @IBInspectable var shadowOpacity: Float = 0.3
  
  override func layoutSubviews() {
    
    layer.cornerRadius = cornerRadius
    let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
    layer.masksToBounds = false
    layer.shadowColor = shadowColor?.cgColor
    layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
    layer.shadowOpacity = shadowOpacity
    layer.shadowPath = shadowPath.cgPath
  }
  

}
