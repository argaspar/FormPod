//
//  EmptySegue.swift
//  SmartApp
//
//  Created by Karen Sidney on 28/08/17.
//  Copyright © 2017 SURA. All rights reserved.
//
//  Se utiliza en los segues de los contenedores para redefinir el UIStoryboardSegue

import UIKit

class SAEmptySegue: UIStoryboardSegue {
    override func perform() {
    }
}
