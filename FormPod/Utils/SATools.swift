//
//  SATools.swift
//  SmartApp
//
//  Created by Karen Sidney on 06/08/17.
//  Copyright © 2017 SURA. All rights reserved.
//  Herramientas

import Foundation
import UIKit
import RealmSwift

class SATools {

  // MARK: Color
  
  /// Método genérico para cambiar las vista a un color claro
  ///
  /// - Parameter auxViews: Arreglo de vistas
  class func changeToClearColorTheBackgroundOfTheViews(_ viewsAuxiliars: Array<UIView>) {
      for viewAux: UIView in viewsAuxiliars {
        viewAux.backgroundColor = UIColor.clear
      }
  }
  
  /// Método genérico para cambiar el color del texto y la fuente de una etiqueta
  ///
  /// - Parameter labelTitle: Etiqueta Titulo
  /// - Parameter keyString: Lleve del texto a buscar en el archivo .strings
  class func changeColorAndFontOfLabelMainTitleAndSetText(_ title:UILabel , _ keyString: String){
      
      title.backgroundColor = UIColor.clear
      title.textColor = UIColor.init(hex: SATextNameBlackColorCode).withAlphaComponent(1.0)
      title.text = NSLocalizedString(keyString, comment: "Text Title of Label")
      title.font = UIFont(name: kSAFamilyFontDINBold, size: 25)
      title.adjustsFontSizeToFitWidth = true
      
  }
  

  /// Método genérico para cambiar el color del texto y la fuente de una etiqueta
  ///
  /// - Parameters:
  ///   - title:  Etiqueta Titulo
  ///   - keyString: Lleve del texto a buscar en el archivo .strings
  ///   - textColor: Color del texto
  class func changeColorAndFontOfLabelTitleAndSetText(_ title:UILabel , _ keyString: String, _ textColor:UIColor){
  
      title.backgroundColor = UIColor.clear
      title.textColor = textColor
      title.text = NSLocalizedString(keyString, comment: "Text Title of Label")
      title.font = UIFont(name: kSAFamilyFontDINBold, size: 20)
    
  }
  
 
  /// Método genérico para cambiar el color del texto y la fuente de una etiqueta
  ///
  /// - Parameters:
  ///   - labelSubtitle: Etiqueta Titulo
  ///   - textColor: Color
  ///   - size: Tamaño de texto
  class func changeColorAndFontOfLabelSubtitle(_ labelSubtitle:UILabel,  _ textColor:UIColor, _ size: CGFloat){

      labelSubtitle.backgroundColor = UIColor.clear
      labelSubtitle.textColor = textColor
      labelSubtitle.font = UIFont(name: kSAFamilyFontDINRegular, size: size)
      labelSubtitle.text = ""
      
  }
  
  /// Método genérico para cambiar el color del fondo de un botón. Y setea el texto.
  ///
  /// - Parameter button: Botón a cambiar su color de fondo
  /// - Parameter keyString: Llave del texto a buscar en el archivo .strings
  class func setColorGrayFontAndTextButton(_ button :UIButton, _ keyString: String){
    
    button.titleLabel?.textColor = UIColor.init(hex: SAColor526376)
    let title = NSLocalizedString(keyString, comment: "Text Title of button")
    button.setTitle(title, for: .normal)
    button.titleLabel?.font = UIFont(name: kSAFamilyFontDINBold, size: 20)

  }
  

  
  /// Método genérico para cambiar el color del fondo de un botón. Y setea el texto
  ///
  /// - Parameter button: Botón a cambiar su color de fondo
  /// - Parameter keyString: Lleve del texto a buscar en el archivo .strings
  class func changeBackgroundColorButtonAndSetText(_ button :UIButton, _ keyString: String){
      
      button.titleLabel?.textColor = UIColor.white
      button.titleLabel?.font = UIFont(name: kSAFamilyFontDINBold, size: 16)
      let title = NSLocalizedString(keyString, comment: "Text Title of button")
      button.setTitle(title, for: .normal)
      button.backgroundColor = UIColor.init(hex: SAGenericBlueColorOfLineOrButton).withAlphaComponent(1.0)
      
  }
  
  /// Método genérico para cambiar el color del fondo de un botón. Y setea el texto
  ///
  /// - Parameter button: Botón a cambiar su color de fondo
  /// - Parameter keyString: Lleve del texto a buscar en el archivo .strings
  class func clearBackgroundColorButtonAndSetText(_ button :UIButton, _ keyString: String){
    
    button.titleLabel?.textColor = UIColor.init(hex: SAColor00226C)
    button.titleLabel?.font = UIFont(name: kSAFamilyFontDINRegular, size: 20)
    let title = NSLocalizedString(keyString, comment: "Text Title of button")
    button.setTitle(title, for: .normal)
    button.backgroundColor = UIColor.clear
    
  }
    
  //MARK: - Guarda el archivo
  ///
  /// - Parameters:
  ///   - nameOfFile: Nombre del archivo
  ///   - tempDirectory: Carpeta temporal
  class func saveDownloadedFile(nameOfFile: String, tempDirectory: String) -> Bool{
    return SAFilesManager.saveFileIntoDirectory(nameOfFile: nameOfFile, nameDirectory: AppVariables.paysheetForScreen, tempDirectory: tempDirectory)
  }
  
  
  //MARK: - Verifica si existe la carpeta
  ///
  /// - Parameters:
  /// - folderName: Nombre de la carpeta
  /// - Returns: true si existe, de lo contrario no
  class func folderExists(folderName: String) -> Bool {
    let directory = SAFilesManager.getPathFilesDirectory(path:folderName)
    return directory == nil ? false : true
  }

  
  //MARK: - Guarda la imagen
  
  
  /// Guarda la imagen en documentos/imagenes/...
  ///
  /// - Parameter imageCapture: Imagen a guardar
  class func captureAndSaveImage(imageCapture:UIImage) {
    
    var finalImage: UIImage =  imageCapture
    finalImage = finalImage.resized(withPercentage: 0.5)! // Se bajo la calidad de la imagen
    let nameFile = self.getNameOfFile()
    SAFilesManager.saveImageIntoDirectory(imageToSave: finalImage, nameOfImage: nameFile, nameDirectory: AppVariables.paysheetForScreen)
  }
  

  /// Guarda la imagen en documentos/imagenes/...
  ///
  /// - Parameter capture: Imagen a guardar
  /// - Returns: Regresa la url donde esta almacenada la imagen
  class func captureSaveImageAndGetURL(capture:UIImage) -> URL {
    
    var url = URL(string:"")
    var finalImage: UIImage = capture
    finalImage = finalImage.resized(withPercentage: 0.5)! // Se bajo la calidad de la imagen
    let nameFile = self.getNameOfFile()
    
    url = SAFilesManager.saveImageIntoDirectoryAndGetURL(imageToSave: finalImage, nameOfImage: nameFile, nameDirectory: AppVariables.paysheetForScreen)
    return url!
  }
  
  
  //MARK: - Obtiene el nombre de la imagen
  
  /// Obtiene el nombre de la image
  ///
  /// - Returns: Nombre de la imagen
  class func getNameOfFile()-> String{
    
    let dateString: String  = SATools.convertDateCurrentToString()!
    let nameMoreTime = AppVariables.paysheetForScreen + "_time_" + dateString + ".png"
    return nameMoreTime
  }

  //MARK: - Fechas
  
  /// Convierte una Fecha en cadena
  ///
  /// - Returns: Regresa la fecha en cadena
  class func convertDateCurrentToString() -> String? {
    
    let date = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
    formatter.locale = Locale(identifier: SALocaleMX)
    let defaultTimeZoneStr = formatter.string(from: date)  // "2014-07-23 11:01:35 -0700" <-- misma fecha, local, pero con segundos
    return  defaultTimeZoneStr
  }
    
    
  /// Convierte una cadena a fecha
  ///
  /// - Parameter dateString: Fecha
  /// - Returns: Regresa la fecha en cadena
  class func convertToDateForImage(dateString:String) -> String {
    
      let formatter = DateFormatter()
      formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
      formatter.locale = Locale(identifier: SALocaleMX)
      let date = formatter.date(from:dateString)
      formatter.timeZone = TimeZone.current
      formatter.dateStyle = .long
      formatter.timeStyle = .short
      let result = formatter.string(from:date!)

      return result
  }
  
  /// Regresa el nombre del mes
  ///
  /// - Parameter month: Mes
  /// - Returns: Regresa el nombre del mes
  class func monthNameByNumber(month: Int) -> String {
    
    if let monthIndex = NumberMonth(rawValue: month){
      switch monthIndex {
      case .January:
        return "Enero"
      case .February:
        return "Febrero"
      case .March:
        return "Marzo"
      case .April:
        return "Abril"
      case .May:
        return "Mayo"
      case .June:
        return "Junio"
      case .July:
        return "Julio"
      case .August:
        return "Agosto"
      case .September:
        return "Septiembre"
      case .October:
        return "Octubre"
      case .November:
        return "Noviembre"
      case .December:
        return "Diciembre"
      }
    }
    return ""
  }
  
  
  
  /// Obtiene la fecha actual
  ///
  /// - Returns: Regresa la fecha actual en string
  class func getStringOfCurrentDateForSorted() -> String{
    
    let SENDEMAIL_DATE_FORMATER = "yyyyy/MM/dd"
    let todaysDate:Date = Date()
    let dateFormatter:DateFormatter = DateFormatter()
    dateFormatter.dateFormat = SENDEMAIL_DATE_FORMATER
    dateFormatter.locale = Locale(identifier:SALocaleMX)
    let todayString =  dateFormatter.string(from: todaysDate)
    
    return todayString
  }
  
  /// Obtiene la fecha actual
  ///
  /// - Parameter completion: Regresa la fecha actual
  class func getCurrentDate(completion: @escaping ( _ year: Int, _ month:Int, _ day:Int) -> Void ) {
    
    let date = Date()
    let calendar = Calendar.current
    
    let year = calendar.component(.year, from: date)
    let month = calendar.component(.month, from: date)
    let day = calendar.component(.day, from: date)
    completion(year,month,day)
    
  }
  
  ///  Regresa la fecha en string
  ///
  /// - Returns: Regresa la fecha actual
  class func covertDateToString(date:Date) ->String?{
    
    let dateFormatter:DateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    dateFormatter.locale = Locale(identifier: SALocaleMX)
    let stringDate =  dateFormatter.string(from: date)
    return stringDate
  }
  

  /// Obtiene la fecha actual
  ///
  /// - Returns: Regresa la fecha actual en string
  class func getStringOfCurrentDate() -> String{
    
    let SENDEMAIL_DATE_FORMATER = "dd/MM/yyyy"
    let todaysDate:Date = Date()
    let dateFormatter:DateFormatter = DateFormatter()
    dateFormatter.dateFormat = SENDEMAIL_DATE_FORMATER
    dateFormatter.locale = Locale(identifier: SALocaleMX)
    let todayString =  dateFormatter.string(from: todaysDate)
    
    return todayString
  }
  
  
  /// Obtiene el año en curso
  ///
  /// - Returns: Regresa el año actual
  class func getCurrentYear() -> Int {
    let date = Date()
    let calendar = Calendar.current
    let year = calendar.component(.year, from: date)
    return year
  }
  

  /// Convierte una cadena a fecha y obtiene el año
  ///
  /// - Parameter stringDate: Fecha
  /// - Returns: Regresa el año
  class func convertStringToDateAndGetYear(stringDate: String) -> String {
    
    var yearStringWithFormat = ""
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    let date = dateFormatter.date(from:stringDate)!
    dateFormatter.dateFormat = "YYYY"
    yearStringWithFormat = dateFormatter.string(from:date)
    
    return yearStringWithFormat
  }
  
  /// Convierte una cadena a fecha y obtiene el mes
  ///
  /// - Parameter stringDate: Fecha
  /// - Returns: Regresa el mes
  class func convertStringToDateAndGetMonth(stringDate: String) -> String {
    
    var monthStringWithFormat = ""
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    let date = dateFormatter.date(from:stringDate)!
    dateFormatter.dateFormat = "MM"
    monthStringWithFormat = dateFormatter.string(from:date)
    
    return  monthStringWithFormat
  }

  /// Regresa la fecha con formato
  ///
  /// - Parameter stringDate: Fecha
  /// - Returns: Regresa la fecha con formato para ordenar
  class func formatterCorrectForSorted(stringDate: String) -> String {
    
    var monthStringWithFormat = ""
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    let date = dateFormatter.date(from:stringDate)!
    dateFormatter.dateFormat = "yyyy/MM/dd"
    monthStringWithFormat = dateFormatter.string(from:date)
    
    return  monthStringWithFormat
  }

  //MARK: - Capturas
  
  /// Generar imagen de captura de pantalla
  ///
  /// - Parameter tblview: Tabla
  /// - Returns: Regresa al imagen
  class func generateImage(tblview:UITableView) ->UIImage{
        
        UIGraphicsBeginImageContext( tblview.contentSize )
        tblview.scrollToRow(at: NSIndexPath(row: 0, section: 0) as IndexPath, at:   UITableViewScrollPosition.top, animated: false)
        tblview.layer.render(in: UIGraphicsGetCurrentContext()!)
        let row = tblview.numberOfRows(inSection: 0)
        let numberofRowthatShowinscreen = 4
        let scrollCount = row / numberofRowthatShowinscreen
        for i in 0 ..< scrollCount  {
            tblview.scrollToRow(at: NSIndexPath(row: (i+1)*numberofRowthatShowinscreen, section: 0) as     IndexPath, at: UITableViewScrollPosition.top, animated: false)
            tblview.layer.render(in: UIGraphicsGetCurrentContext()!)
        }
    
        let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
  }
    
    
  /// Generar imagen de captura de pantalla
  ///
  /// - Parameter collectionview: UICollectionView
  /// - Returns: Regresa al imagen
  class func  generateImage(collectionview: UICollectionView)  -> UIImage{
    
      UIGraphicsBeginImageContext(collectionview.contentSize )
      collectionview.scrollToItem(at: NSIndexPath(row: 0, section: 0) as IndexPath, at: .top, animated: false)
      collectionview.layer.render(in: UIGraphicsGetCurrentContext()!)
      let row = collectionview.numberOfItems(inSection: 0)
      let numberofRowthatShowinscreen = 4
      let scrollCount = row / numberofRowthatShowinscreen
      for i in 0 ..< scrollCount  {
          collectionview.scrollToItem(at: NSIndexPath(row: (i+1)*numberofRowthatShowinscreen, section: 0) as IndexPath, at: .top, animated: false)
          collectionview.layer.render(in: UIGraphicsGetCurrentContext()!)
      }
    
      let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
      UIGraphicsEndImageContext()
      return image
  }


  
  //MARK: Mostrar botón menu

  /// Mostrar o ocultar el botón de back
  ///
  /// - Parameters:
  ///   - show: Muestra o oculta el menu
  ///   - viewMenu: Vista del menu
  class func showOrHideMenuLateral(show:Bool, viewMenu:HamburgerView?) {
      var userInfo = [String:String]()
      if show {
          userInfo["hideOrShowMenuLateral"] = "true"
      }else {
          userInfo["hideOrShowMenuLateral"] = "false"
      }
    if let menu = viewMenu {
      // Post notification
      NotificationCenter.default.post(name: .menuOptionNotificationName , object:menu , userInfo: userInfo)
    }else{
      NotificationCenter.default.post(name: .menuOptionNotificationName , object:nil, userInfo: userInfo)
    }
    
  }
    
  /// Mostrar o ocultar el botón de back
  ///
  /// - Parameters:
  ///   - show: Muestra o oculta el menu
  ///   - viewMenu: Vista del menu
  
  
  /// Se actualiza la lista del menu
  ///
  /// - Parameter optionSelected: Opción de la lista a mostrar
  class func updateListMenu(optionSelected:Int) {
    var userInfo = [String:Int]()
    userInfo["OptionList"] = optionSelected

    // Post notification
    NotificationCenter.default.post(name: .menuListOptionsNotificationName, object: nil, userInfo: userInfo)
  }
  
  //MARK: Obtiene respuestas de preguntas
  
  /// Obtiene el arrreglo para enviarse al servicio
  ///
  /// - Returns: Regresa la lista de respuestas
  /*class func getArrayForService(arraySAQuestions:[SAResponseQuestion]) -> Array<Any>{
    var arrayService: Array<Any> = []
    
    for item in arraySAQuestions {
      
        let parameters = ["idPregunta": item.idPregunta, "respuesta":item.respuesta, "detalle_respuesta":item.comments] as  [String : Any]
      arrayService.append(parameters)
    }
    
    return arrayService
  }*/
  
  //MARK: - Alerta génerica
  
  /// Muestra Alerta
  ///
  /// - Parameters:
  ///   - title: Título para la alerta
  ///   - message: Mensaje para la alerta
  ///   - vc: Controlador donde se mostrará la alerta
  class func showAlert(withTitle title:String , withMessage message: String, AndVC vc: UIViewController) {
    
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
    
    let okaction = UIAlertAction(title: NSLocalizedString(kSAStringMessageOK, comment: ""), style: UIAlertActionStyle.cancel, handler:nil)
    alert.addAction(okaction)
    
    vc.present(alert, animated: true, completion: nil)
    
  }
  
  /// Muestra Alerta
  ///
  /// - Parameters:
  ///   - title: Título para la alerta
  ///   - message: Mensaje para la alerta
  ///   - url: Página que se mostrará al abrir safari
  ///   - vc: Controlador donde se mostrará la alerta
  class func showAlert(withTitle title:String , withMessage message: String, withURL webSite: String, AndVC vc: UIViewController) {
    
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
    
    let ok = NSLocalizedString(kSAAccept, comment: kSAStringMessageAlert)
    let okAction = UIAlertAction(title: ok, style: UIAlertActionStyle.default, handler:
    { action in
      if let url = URL(string: webSite) {
        UIApplication.shared.open(url, options: [:])
      }
    })
    
    let cancelAction = UIAlertAction(title: NSLocalizedString(kSATitleCancel, comment: kSAStringComment), style: .cancel, handler: { (action) -> Void in
      
    })
    
    alert.addAction(okAction)
    alert.addAction(cancelAction)
    
    vc.present(alert, animated: true, completion: nil)
    
  }
  

  /// Muestra un mensaje y retorna a la vista anterior
  ///
  /// - Parameters:
  ///   - vc: UIViewController
  ///   - messageKey: Llave del mensaje a mostrar
  class func gotoBackviewAndShowAlert(vc:UIViewController, messageKey: String) {
    
    let title = NSLocalizedString(kSATitleNotice, comment: "")
    let message = NSLocalizedString(messageKey, comment: "")
    let ok = NSLocalizedString(kSAStringMessageOK, comment: kSAStringMessageAlert)
    
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let okaction = UIAlertAction(title: ok, style: UIAlertActionStyle.default, handler:
    { action in
      alert.dismiss(animated: false, completion: nil)
      vc.navigationController?.popViewController(animated: true)
    })
    alert.addAction(okaction)
    vc.present(alert, animated: true, completion: nil)
  }
 
  
  // MARK: - Obtener el nombre del usuario

  /// Nombre del usuario logueado
  ///
  /// - Returns: Regresa el nombre del usuario logueado
//  class func getNameUserOfDB() -> String {
//     let userPersistence = SAUserPersistenceManager()
//     let nameUser  = userPersistence.getUser().displayName
//     return nameUser
//  }
 
  // MARK: - Obtener la dirección IP
  
  /// Devuelve la dirección IP de la interfaz WiFi (en0) como String o `nil`
  ///
  /// - Returns: Regresa la dirección IP
  class func getIPAddress() -> String? {
    
    var address : String?
    
    // Obtener la lista de todas las interfaces en la máquina local:
    var ifaddr : UnsafeMutablePointer<ifaddrs>?
    guard getifaddrs(&ifaddr) == 0 else { return nil }
    guard let firstAddr = ifaddr else { return nil }
    
    // Por cada interfaz ...
    for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
      let interface = ifptr.pointee
      
      // Comprueba la interfaz IPv4 o IPv6:
      let addrFamily = interface.ifa_addr.pointee.sa_family
      if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
        
        // Comprueba el nombre de la interfaz:
        let name = String(cString: interface.ifa_name)
        if  name == "en0" || name == "pdp_ip0"{
          
          // Convertir la dirección de la interfaz en una cadena legible:
          var addr = interface.ifa_addr.pointee
          var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
          getnameinfo(&addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                      &hostname, socklen_t(hostname.count),
                      nil, socklen_t(0), NI_NUMERICHOST)
          address = String(cString: hostname)
        }
      }
    }
    freeifaddrs(ifaddr)
    
    return address
  }
  
  //MARK: - Validar email
  
  /// Validar email
  ///
  /// - Parameter testStr: Cadena de string
  /// - Returns: Regresa true si es valida
  class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
  }
  
  //MARK: - Validar meses
  
  /// Regresa si el ejecutivo tiene una antiguedad mayor a 3 meses
  ///
  /// - Parameter dateString: Fecha de inicio
  /// - Returns: Regresa Sí si tiene una antiguedad mayor a 3 meses
  class func isGreaterThanThreeMonths(dateString:String) -> Bool {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
 
    let startDate = dateFormatter.date(from: dateString)
    
    let date = Date()
    let calendar = Calendar.current
    
    // Replace the hour (time) of both dates with 00:00
    let date1 = calendar.startOfDay(for: startDate!)
    let date2 = calendar.startOfDay(for: date)
    
    let components = calendar.dateComponents([.month], from: date1, to: date2)
    let months:Int = components.month!
    if months > 3 {
      return true
    }else {
      return false
    }
    
  }
  
  /// Regresa si la fecha seleccionada del periodo es mayor a 2 meses
  ///
  /// - Parameter dateString: Fecha de inicio
  /// - Returns: Regresa Sí si es mayor a 2 meses
  class func isGreaterThanTwoMonths(dateString:String) -> Bool {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
  
    if  let startDate = dateFormatter.date(from: dateString),  let monthsAgo = Calendar.current.date(byAdding: .month, value: -2, to: Date()) {
      if startDate > monthsAgo || startDate == monthsAgo {
         return true
      }else {
        return false
      }
    }else {
      return false
    }

  }
  
  //MARK: - Cambia el formato de una cadena
  
  /// Cambia el formato de una cadena . Mayusculas a mínusculas
  ///
  /// - Parameter titleString: Cadena a cambiar el formato
  /// - Returns: Regresa cadena con formato
  class func titleFormatter(titleString : String? ) -> String{
    
    if var stringToFormat = titleString{
      stringToFormat = stringToFormat.lowercased()
      guard let first = stringToFormat.first else { return "" }
      stringToFormat = String(first).uppercased() + stringToFormat.dropFirst()
      let finalString = stringToFormat.replacingOccurrences(of: "[ |_-]", with: " ", options: .regularExpression)
      
      return finalString.capitalized
    }
    return ""
  }
 
  //MARK: - Nomina
  
  /// Obtiene la nomina correcta
  ///
  /// - Parameter userID: Nomina
  /// - Returns: Regresa la nomina correcta
  class func changeUserID(userID:String)->String?{
    
    switch (userID){
      case "95894000001":
        return "958940"
      case "10155749990":
        return "1015574"
      case "10165030001":
        return "1016503"
      case "90008476001":
        return "90008476"
      default:
        return userID
      }
  }
  
  class func getValueFromCSV(nomina: String, option: csvOption) -> String {
    
    let path:String = Bundle.main.path(forResource: kSAPlan, ofType: KSAExtensionCSV)!
    
    let content:String = try! String(contentsOfFile: path, encoding: String.Encoding.utf8)
    
    let noReturnSymbol = content.replacingOccurrences(of: "\r", with: "")
    
    let csvParsed: [[String]] = noReturnSymbol.components(separatedBy: "\n").map{$0.components(separatedBy: ",")}
    
    let filter = csvParsed.filter{item in
        return item[0].contains(nomina)
    }
    
    if let result = filter.first {
        let value = result[result.count - option.rawValue]
        return value
    }
    
    return ""
    
  }
  
  class func getColorName(hex: String) -> String {
    
    switch hex {
    case SAColorSemaphoreRed:
      return SAColorNameRED
      
    case SAColorSemaphoreYellow:
      return SAColorNameYELLOW
      
    case SAColorSemaphoreOrange:
      return SAColorNameORANGE
      
    case SAColorSemaphoreGreen:
      return SAColorNameGREEN
      
    default:
      return SAColorNameBLUE
    }
  
  }
  
}

