//
//  ProgressCircle.swift
//  SmartApp
//
//  Created by Karen Sidney on 16/08/17.
//  Copyright © 2017 SURA. All rights reserved.
//
//  Círculo de Progreso

import UIKit
import QuartzCore


class SAProgressCircleView: UIView {
  
  /// Texto a mostrar en el Círculo
  public var textValue: String = ""
  /// Valor a mostrar en el Círculo
  public var value :CGFloat = 0.0 {
    didSet {
      refreshValues()
    }
  }
  /// Color del texto
  public var colorLabel:UIColor = UIColor.black {
     didSet {
       progressLabel.textColor = colorLabel
    }
  }
    

  /// Máximo valor a mostrar
  fileprivate var maxValue : CGFloat = 100.0
  /// Color del círculo
  fileprivate var colorLayerCircle : UIColor = UIColor.init(hex: SAColorCircleNormal)
  /// Color del círculo de progreso
  fileprivate var colorLayerProgress : UIColor = UIColor.init(hex: SAColorCircleProgress)
  //Etiqueta donde su mostrará el progreso
  fileprivate var progressLabel: UILabel = UILabel()
  // La capa de forma gris
  fileprivate var progressLayerCircle: CAShapeLayer = CAShapeLayer()
  // Segunda capa de forma azul
  fileprivate var progressLayerProgress: CAShapeLayer = CAShapeLayer()
  // Primer capa de forma azul
  fileprivate var firstProgressLayerProgress: CAShapeLayer = CAShapeLayer()
  // El radio del círculo
  fileprivate var radius: CGFloat = 0.0
  // El ángulo final
  fileprivate var endAngle:CGFloat = 0.0
  // El ángulo inicial
  fileprivate let startAngle: CGFloat = CGFloat(-CGFloat.pi/2)
  // En progreso final
  fileprivate let endAngleLayerGray: CGFloat =  CGFloat(3 * CGFloat.pi/2)
  // El centro del círculo
  fileprivate var centerPoint: CGPoint = CGPoint(x: 0, y: 0)
 
  // MARK: - Configuración de la vista
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    translatesAutoresizingMaskIntoConstraints = false
    createProgressLayer()
    createLabel()
    setInitialValues()
    refreshValues()
  }
  
  override public init(frame: CGRect) {
    super.init(frame: frame)
    createProgressLayer()
    createLabel()
    setInitialValues()
    refreshValues()

  }

  public override func layoutSubviews() {
    super.layoutSubviews()
    updateFrames()
    refreshValues()
    progressLayerProgress.setNeedsDisplay()
    progressLayerCircle.setNeedsDisplay()
    firstProgressLayerProgress.setNeedsDisplay()
  }
  
  
  /// Establece el color de fondo de la vista
  private func setInitialValues() {
    backgroundColor = UIColor.white
  }
  
  public override func prepareForInterfaceBuilder() {
    setInitialValues()
    refreshValues()
    progressLayerProgress.setNeedsDisplay()
    progressLayerCircle.setNeedsDisplay()
    firstProgressLayerProgress.setNeedsDisplay()
  }
  
  /// Refresca los valores
  private func refreshValues() {
    progressLabel.text = textValue
    progressLabel.textColor = colorLabel
    endAngle = degreesValueToRadians(valueToConvert: value)
    progressLayerProgress.strokeColor = colorLayerProgress.cgColor
    progressLayerCircle.strokeColor = colorLayerCircle.cgColor
    firstProgressLayerProgress.strokeColor = colorLayerProgress.cgColor

   
  }
  
  /// Crea la etiqueta donde mostrará el tetxo
  fileprivate func createLabel() {
 
    let font: UIFont;
    
    switch UIDevice.current.userInterfaceIdiom {
      case .phone: //  iPhone
        progressLabel = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: 21))
        font = UIFont(name: kSAFamilyFontDINMedium, size: 36.0)!
        break
      case .pad: //  iPad
        progressLabel = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: 21))
        font = UIFont(name: kSAFamilyFontDINMedium, size: 25.0)!
        break
      default:
        progressLabel = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: 15))
        font = UIFont(name: kSAFamilyFontDINMedium, size: 8.0)!
        break
    
    }
    
    progressLabel.font = font
    progressLabel.textColor = colorLabel
    progressLabel.textAlignment = NSTextAlignment.center
    progressLabel.translatesAutoresizingMaskIntoConstraints = false
    progressLabel.adjustsFontSizeToFitWidth = true
    
    addSubview(progressLabel)
    
    addConstraint(NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: progressLabel, attribute: .centerX, multiplier: 1.0, constant: 0.0))
    addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: progressLabel, attribute: .centerY, multiplier: 1.0, constant: 0.0))

  }
  
  
  /// Crea el círculo de Progresp
  fileprivate func createProgressLayer() {
    
      centerPoint = CGPoint(x:bounds.size.width/2, y:bounds.size.height/2)
      radius = getTheRadiusOfTheCircle() - 8

      progressLayerCircle.path = UIBezierPath(arcCenter:centerPoint, radius: radius, startAngle:startAngle, endAngle:endAngleLayerGray , clockwise: true).cgPath
      progressLayerCircle.fillColor = UIColor.clear.cgColor
      progressLayerCircle.strokeColor = colorLayerCircle.cgColor
      progressLayerCircle.lineWidth = 10.0
      progressLayerCircle.strokeStart = 0.0
      progressLayerCircle.strokeEnd = 1.0

      layer.addSublayer(progressLayerCircle)

      endAngle = degreesValueToRadians(valueToConvert: value)

      // Relleno
      progressLayerProgress.path = UIBezierPath(arcCenter:centerPoint, radius: radius , startAngle:startAngle, endAngle:endAngle, clockwise: true).cgPath
      progressLayerProgress.fillColor = UIColor.clear.cgColor
      progressLayerProgress.strokeColor = colorLayerProgress.cgColor
      progressLayerProgress.lineWidth = 10.0
      progressLayerProgress.strokeStart = 0.0
      progressLayerProgress.strokeEnd = 1.0

      layer.addSublayer(progressLayerProgress)
    
      // Layer externo
      let newradius = getTheRadiusOfTheCircle()
    
      firstProgressLayerProgress.path = UIBezierPath(arcCenter:centerPoint, radius: newradius , startAngle:startAngle, endAngle:endAngle, clockwise: true).cgPath
      firstProgressLayerProgress.fillColor = UIColor.clear.cgColor
      firstProgressLayerProgress.strokeColor = colorLayerProgress.cgColor
      firstProgressLayerProgress.lineWidth = 3.0
      firstProgressLayerProgress.strokeStart = 0.0
      firstProgressLayerProgress.strokeEnd = 1.0
    
      layer.addSublayer(firstProgressLayerProgress)
  }
  

  
  /// Obtiene el radio de un círculo
  ///
  /// - Returns: Devuelve el radio para crear un círculo
  fileprivate func getTheRadiusOfTheCircle() -> CGFloat {
    let minValue =  min(bounds.size.width, bounds.size.height)
    let measure: CGFloat =  minValue/2.0 //* 0.8  //Se comento para que abarcará toda la vista
    return measure;
  }


  /// Actualiza los frames
  func updateFrames() {
    
    radius = getTheRadiusOfTheCircle() - 8
    centerPoint = CGPoint(x:bounds.size.width/2, y:bounds.size.height/2)
    endAngle = degreesValueToRadians(valueToConvert: value)
    
    progressLayerCircle.path = UIBezierPath(arcCenter:centerPoint, radius: radius, startAngle:startAngle, endAngle:endAngleLayerGray , clockwise: true).cgPath
    
    progressLayerProgress.path = UIBezierPath(arcCenter:centerPoint, radius: radius , startAngle:startAngle, endAngle:endAngle, clockwise: true).cgPath
    
    let newradius = getTheRadiusOfTheCircle()
    firstProgressLayerProgress.path = UIBezierPath(arcCenter:centerPoint, radius: newradius , startAngle:startAngle, endAngle:endAngle, clockwise: true).cgPath

    
  }
  
  
  /// Convierte un valor a radianes
  ///
  /// - Parameter valueToConvert: Valor a convertir
  /// - Returns: Regresa el angulo inicial
  fileprivate func degreesValueToRadians (valueToConvert: CGFloat) -> CGFloat {
   // Calcular la diferencia central entre el ángulo final y el ángulo inicial
    let angleDiff: CGFloat = endAngleLayerGray - startAngle
    // Calcula cuánto debemos dibujar dependiendo del valor establecido
    let arcLenPerValue = angleDiff / CGFloat(maxValue)
    // El ángulo final interno
    let innerEndAngle  = arcLenPerValue * CGFloat(valueToConvert) + startAngle
    
    return innerEndAngle
  }
  
  
  ///  Obtiene la fuente de una cadena
  ///
  /// - Parameters:
  ///   - valueString:  La cadena a evaluar
  ///   - rect: El marco de la etiqueta
  ///   - seedFont: La fuente a establecer
  /// - Returns: Devuelve las fuente
  func getFontForString(valueString: String , rect:CGRect , seedFont: UIFont) -> UIFont {
    
    var returnFont: UIFont = seedFont
    let nsString: NSString = valueString as NSString
    var stringSize: CGSize = nsString.size(withAttributes: [NSAttributedStringKey.font: seedFont])
    
  
    while stringSize.width > rect.size.width {
      returnFont = UIFont.systemFont(ofSize: returnFont.pointSize-1)
      stringSize = nsString.size(withAttributes: [NSAttributedStringKey.font:returnFont])
    }
    
    return returnFont
  }

  

}
