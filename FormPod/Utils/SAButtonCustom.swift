//
//  SAButtonCustom.swift
//  SmartApp
//
//  Created by Karen Sidney on 30/08/17.
//  Copyright © 2017 SURA. All rights reserved.
//
//  Clase que crea un botón con bordes

import UIKit


@IBDesignable class SAButtonCustom: UIButton {

  /// Anchura del Borde
  @IBInspectable var borderWidth: CGFloat = 2.0 {
    didSet {
      layer.borderWidth = borderWidth
    }
  }

  /// Color del borde del botón en esatdo normal
  @IBInspectable var normalBorderColor: UIColor? {
      didSet {
        layer.borderColor = normalBorderColor?.cgColor
      }
  }

 
  /// Color del fondo del botón
  @IBInspectable var normalBackgroundColor: UIColor? {
      didSet {
       setBgColorForState(color: normalBackgroundColor, forState: .normal)
      }
  }
  
  
  //Highlighted - color del borde
  @IBInspectable var highlightedBorderColor: UIColor?
  //Highlighted - color del fondo
  @IBInspectable var highlightedBackgroundColor: UIColor? {
      didSet {
      setBgColorForState(color: highlightedBackgroundColor, forState: .highlighted)
      }
  }
  

 override func layoutSubviews() {
  super.layoutSubviews()
  
      titleLabel?.font = UIFont(name: kSAFamilyFontDINMedium, size: 16)
      if state == .normal && (layer.borderColor == normalBorderColor?.cgColor) {
          titleLabel?.textColor = normalBorderColor
          layer.borderColor = normalBorderColor?.cgColor
      } else if state == .highlighted && highlightedBorderColor != nil{
          titleLabel?.textColor = highlightedBorderColor
          layer.borderColor = highlightedBorderColor!.cgColor
      }
     clipsToBounds = true
    
  }

  
  /// Setea el color dependiendo el estado
  ///
  /// - Parameters:
  ///   - color: UIColor
  ///   - forState: UIControlState
  private func setBgColorForState(color: UIColor?, forState: UIControlState){
    guard (color != nil) else {
      setTitleColor(color, for: forState)
      setBackgroundImage(UIImage.imageWithColor(color: color!, size: self.bounds.size), for: forState)
      return
    }
    
  }
  

}

// MARK: - Extensión requerida por RoundedButton para crear UIImage de UIColor
extension UIImage {
  
  
  /// Regresa la imagen que contendra el color para redondear el botón
  ///
  /// - Parameters:
  ///   - color: Color
  ///   - size: Medida
  /// - Returns: Imagen
  class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {

      
      let rect: CGRect = CGRect(x: 0.0, y: 0.0, width: max(size.width, 1.0), height: max(size.height, 1.0))
      UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
      let context = UIGraphicsGetCurrentContext()
      
      context!.setFillColor(color.cgColor)
      context!.fill(rect)
      
      let image = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
      
      return image!
      
  }


}

