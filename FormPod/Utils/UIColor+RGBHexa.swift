//
//  UIColor+RGB.swift
//  SmartApp
//
//  Created by Karen Sidney on 06/08/17.
//  Copyright © 2017 SURA. All rights reserved.
//
//  Clase que ayudan a agregar colores por hexadecimal o RGB

import UIKit

extension UIColor {
  
  
  /// Color por RGB
  ///
  /// - Parameters:
  ///   - red: Color Rojo
  ///   - green: Color Verde
  ///   - blue: Color Azul
  convenience init(red: Int, green: Int, blue: Int) {
    assert(red >= 0 && red <= 255, "Invalid red component")
    assert(green >= 0 && green <= 255, "Invalid green component")
    assert(blue >= 0 && blue <= 255, "Invalid blue component")
    
    self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
  }
  
  
  /// RGB con un valor entero
  ///
  /// - Parameter rgb: Entero que representa RGB
  convenience init(rgb: Int) {
    self.init(
      red: (rgb >> 16) & 0xFF,
      green: (rgb >> 8) & 0xFF,
      blue: rgb & 0xFF
    )
  }
  
  
  /// Color por RGB con opacidad
  ///
  /// - Parameters:
  ///   - red: Color Rojo
  ///   - green: Color Verde
  ///   - blue: Color Azul
  ///   - a: Opacidad
  convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
    self.init(
      red: CGFloat(red) / 255.0,
      green: CGFloat(green) / 255.0,
      blue: CGFloat(blue) / 255.0,
      alpha: a
    )
  }
  
  
  /// RGB con un valor entero, con opacidad
  ///
  /// - Parameters:
  ///   - rgb: Valor RGB
  ///   - a: Opacidad
  convenience init(rgb: Int, a: CGFloat = 1.0) {
    self.init(
      red: (rgb >> 16) & 0xFF,
      green: (rgb >> 8) & 0xFF,
      blue: rgb & 0xFF,
      a: a
    )
  }
  
  
  /// Color en hexadecimal
  ///
  /// - Parameter hex: Cadena con valor del color en hexadecimal
  convenience init(hex: String) {
    let scanner = Scanner(string: hex)
    scanner.scanLocation = 0
    
    var rgbValue: UInt64 = 0
    
    scanner.scanHexInt64(&rgbValue)
    
    let r = (rgbValue & 0xff0000) >> 16
    let g = (rgbValue & 0xff00) >> 8
    let b = rgbValue & 0xff
    
    self.init(
      red: CGFloat(r) / 0xff,
      green: CGFloat(g) / 0xff,
      blue: CGFloat(b) / 0xff, alpha: 1
    )
  }

  
  /// Obtiene le RGB
  ///
  /// - Returns: RGB
  func rgb() -> (red:Int, green:Int, blue:Int, alpha:Int)? {
    var fRed : CGFloat = 0
    var fGreen : CGFloat = 0
    var fBlue : CGFloat = 0
    var fAlpha: CGFloat = 0
    if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
      let iRed = Int(fRed * 255.0)
      let iGreen = Int(fGreen * 255.0)
      let iBlue = Int(fBlue * 255.0)
      let iAlpha = Int(fAlpha * 255.0)
      
      return (red:iRed, green:iGreen, blue:iBlue, alpha:iAlpha)
    } else {
      // Could not extract RGBA components:
      return nil
    }
  }
  

  
}
