//
//  SAExtensions.swift
//  SmartApp
//
//  Created by Karen Sidney on 06/09/17.
//  Copyright © 2017 SURA. All rights reserved.
//
//  Clase que contiene extensiones de otras clases

import UIKit
import RealmSwift

//MARK: - Extension de UIImage
extension UIImage {
    
    /// Reduce la calidad de la imagen
    ///
    /// - Parameter percentage: Porcentaje a reducir la calidad de la imagen
    /// - Returns: Regresa una image
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
}

//MARK: - Extension de Dictionary
extension Dictionary where Value:Comparable {
    
    /// Ordenar por valor
    var sortedByValue:[(Key,Value)] {return Array(self).sorted{$0.1 < $1.1}}
}
//MARK: - Extension de Dictionary
extension Dictionary where Key:Comparable {
    
    /// Ordenar por llave
    var sortedByKey:[(Key,Value)] {return Array(self).sorted{$0.0 < $1.0}}
}

extension SACustomAIViewable where Self: UIViewController {
  
  /// Inicia la animación
  public func startAnimating(
        _ size: CGSize = CGSize(width: 30, height: 30),
        message: String? = nil,
        messageFont: UIFont? = nil,
        color: UIColor = .white,
        displayTimeThreshold: Int? = nil,
        minimumDisplayTime: Int? = nil,
        backgroundColor: UIColor? = nil,
        textColor: UIColor? = nil) {
        let data = SAProperties(size: size,
                                message: message,
                                messageFont: messageFont,
                                color: color,
                                displayTimeThreshold: displayTimeThreshold,
                                minimumDisplayTime: minimumDisplayTime,
                                backgroundColor: backgroundColor,
                                textColor: textColor)
        
        SACustomAIPresenter.sharedInstance.startAnimating(data)
    }

  
  /// Detiene la animación
  public func stopAnimating() {
        SACustomAIPresenter.sharedInstance.stopAnimating()
  }
}


//MARK: - Date
extension Date {
  
  /// Regresa el mes
  ///
  /// - Returns: Regresa el nombre del mes
  func monthName() -> String? {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MMMMM"
    dateFormatter.locale = Locale.init(identifier: SALocaleMX)
    return dateFormatter.string(from: self).capitalized
  }
  
}


// MARK: - Notificaciones
extension Notification.Name {
  
  /// Notificación para actualizar la galería
  static let galeryAlbumsNotificationName = Notification.Name("GaleryAlbumsNotificationName")
  /// Notificación para actualizar la galería de sesión 1 a 1
  static let galeryAllImagesNotificationName = Notification.Name("GaleryAllImagesNotificationName")
  /// Notificación para actualizar el menú
  static let menuOptionNotificationName = Notification.Name("MenuNotificationName")
  /// Notificación para actualizar la lista de menú
  static let menuListOptionsNotificationName = Notification.Name("LateraListOptionsNotificationName")
  /// Notificación para tomar una captura de pantalla
  static let screenshotNotificationName = Notification.Name("ScreenShotNotificationName")
  /// Notificación para actualizar el periodo
  static let calendaryChangePeriodNotificationName = Notification.Name("CalendaryChangePeriodNotificationName")
  /// Notificación para actualizar los servicios
  static let updateNotificationName = Notification.Name("UpdateNotificationName")
  
}


// MARK: - Vista
extension UIView {
  
  /// Captura un screen
  ///
  /// - Returns: Regresa una imagen de captura
  func screenshot() -> UIImage {
    
    if(self is UITableView) {
        return SATools.generateImage(tblview: self as! UITableView)
    }else if(self is UICollectionView) {
        return SATools.generateImage(collectionview: self as! UICollectionView)
    }else if (self is UIScrollView){
        let scrollView = self as! UIScrollView
        let savedContentOffset = scrollView.contentOffset
        let savedFrame = scrollView.frame
        
        UIGraphicsBeginImageContext(scrollView.contentSize)
        scrollView.contentOffset = .zero
        self.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        
        scrollView.contentOffset = savedContentOffset
        scrollView.frame = savedFrame
        return image!
    }else {
      
      let layer = UIApplication.shared.keyWindow!.layer
      let scale = UIScreen.main.scale
      UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);

      layer.render(in: UIGraphicsGetCurrentContext()!)
      let image = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()

      return image!
    }

  }
  
    /// Crea un circulo redondeando las esquinas
    ///
    func roundView() {
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }

}

// MARK: - Float
extension CGFloat {
  
  /// Convierte a radianes
  ///
  /// - Returns: Regresa un flotante
  func toRadians() -> CGFloat {
    return self * .pi / 180.0
  }
}

// MARK: - Etiqueta
extension UILabel {
    //Rota el label de acuerdo a los grados que se le manden como parámetro
    ///
    /// - rotation: Número de grados que rotará la etiqueta
    var rotation: Int {
        get {
            return 0
        } set {
            let radians =  CGFloat(newValue).toRadians()
            self.transform = CGAffineTransform(rotationAngle: radians)
        }
    }
}

// MARK: - String

extension String {
    func removeFormatAmount() -> Double {
        let formatter = NumberFormatter()
        
        formatter.locale = Locale(identifier: SALocaleMX)
        formatter.numberStyle = .currency
        formatter.currencySymbol = SASymbolMoney
        formatter.decimalSeparator = ","
        
        return formatter.number(from: self) as! Double? ?? 0
    }
}

// MARK: - Double

extension Double {
    
    func moneyFormat() -> String{
        let formatter = NumberFormatter()
        formatter.currencySymbol = SASymbolMoney
        formatter.minimumFractionDigits = 0
        formatter.numberStyle = .currencyAccounting
        formatter.locale = Locale(identifier: SALocaleMX)
        
        if let string = formatter.string(from: self as NSNumber) {
            return string
        }
        
        return "$0.0"
    }
}

// MARK: - UIViewController

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UITableView {
    
    /// Funcion para agregar un mensaje dentro de la tabla
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}

