//
//  SAEnums .swift
//  SmartApp
//
//  Created by Karen Sidney on 05/09/17.
//  Copyright © 2017 SURA. All rights reserved.
//
//  Se declaran los enum que se usarán en el proyecto

import Foundation


/// Opciones del Menu lateral para vision 12 meses
///
/// - BlackboardMenu: Opción pizarrón
/// - Screenshot:  Opción captura
/// - UpdateMenu:  Opción actualizar
enum MenuLateralOptionsVision: Int {
  case MainMenu = 0
  case BlackboardMenu
  case Screenshot
  case ScreensMenu
  case UpdateMenu
}

/// Opciones del Menu lateral
///
/// - MainMenu:  Opción Menú principal
/// - BlackboardMenu: Opción pizarrón
/// - Screenshot:  Opción captura
/// - ScreensMenu:  Opción galería
enum MenuLateralOptionsDashboard: Int {
  case MainMenu = 0
  case BlackboardMenu
  case Screenshot
  case ScreensMenu
}

/// Opciones del Menu cuando se muestra indicadores smart
///
/// - MainMenu: Opción Menú principal
/// - CalendaryMenu: Opción calendario
/// - BlackboardMenu: Opción pizarrón
/// - Screenshot: Opción captura
/// - ScreensMenu: Opción galería
/// - UpdateMenu: Opción actualizar
enum MenuOptionsWithCalendary: Int {
  case MainMenu = 0
  case CalendaryMenu
  case BlackboardMenu
  case Screenshot
  case ScreensMenu
  case UpdateMenu
}

/// Número de fila
///
/// - ZeroRow: 0
/// - OneRow: 1
/// - TwoRow: 2
/// - ThreeRow: 3
enum NumberRowItemPrincipalMenu: Int {
  case ZeroRow = 0
  case OneRow
  case TwoRow
  case ThreeRow
  case FourRow
}


/// Número de fila
///
/// - ZeroRow: 0
/// - OneRow: 1
/// - TwoRow: 2
/// - ThreeRow: 3
/// - FourRow: 4
enum NumberRowItemForPhone: Int {
  case ZeroRow = 0
  case OneRow
  case TwoRow
  case ThreeRow
}

/// Opciones para el menú de Dashboard
///
/// - IndicatorsSmartOption: Opción indicadores
/// - Session1and1Option: Opción sesión 1 a1
/// - AccompanimentOption: Opción acompañamiento
/// - CheckListOption: Opción checklist
/// - DevelopmentPlan: Opción plan de desarrollo
enum DashboardOptions: Int {
  case Session1and1Option = 0
  case AccompanimentOption
  case CheckListOption
  case ServiceSurvey
  case DevelopmentPlan
}

/// Opciones para el menú de Dashboard Principal
///
enum DashboardPrincipalOptions: Int {
    case IndicatorsSmartOption = 0
    case DevelopmentPlan
    case Tools
    case GroupMeetings
    case Binnacle
}

/// Opciones para el acompañamiento
///
/// - SemaphoreRED: Rojo - 0
/// - SemaphoreYELLOW: Amarillo - 1
/// - SemaphoreGREEN: Verde - 2
/// - SemaphoreWHITE: Blanco - 3
enum SemaphoresOptions: Int {
  case SemaphoreRED = 0
  case SemaphoreYELLOW
  case SemaphoreGREEN
  case SemaphoreWHITE
}

/// Número de mes
///
/// - January: Enero - 1
/// - February: Febrero - 2
/// - March: Marzo - 3
/// - April: Abril - 4
/// - May: Mayo - 5
/// - June: Junio - 6
/// - July: Julio - 7
/// - August: Agosto - 8
/// - September: Septiembre - 9
/// - October: Octubre - 10
/// - November: Noviembre - 11
/// - December: Diciembre - 12
enum NumberMonth: Int {
  case January = 1
  case February
  case March
  case April
  case May
  case June
  case July
  case August
  case September
  case October
  case November
  case December
}


/// Opciones para las solicitudes
///
/// - RequestExecutive: Solicitud de Ejecutivos
/// - RequestQuestions: Solicitud de Preguntas
/// - RequestSaveQuestions: Solicitud de guardar preguntas
enum RequestOptions: Int {
  case RequestExecutive = 1
  case RequestQuestions
  case RequestSaveQuestions
}




/// Opciones para el segmento
///
/// - OptionBtnSave: Opción de Ahorro
/// - OptionBtnNETEO: Opción de NETEO
/// - OptionBtnRCV: Opción de RCV
/// - OptionBtnInsurance: Opción de Seguros
enum OptionsSegmented: Int {
  case OptionBtnSave = 1
  case OptionBtnNETEO
  case OptionBtnRCV
  case OptionBtnInsurance
}


/// Opciones para el gráfica lineal
///
/// - OptionVisionIntegralityTeam: Opción integralidad de equipo
/// - OptionVisionSaves: Opción de Ahorro
/// - OptionBtnInsurance: Opción de Seguros
/// - OptionVisionNETEO: Opción de NETEO
/// - OptionVisionRCV: Opción de RCV
enum OptionsVisionMonths: Int {
    case OptionVisionIntegralityTeam = 0
    case OptionVisionSaves
    case OptionBtnInsurance
    case OptionVisionNETEO
    case OptionVisionRCV
   
}

/// Opciones para el menú de Dashboard
///
/// - DashboardMenu: Menú para dashboard
/// - GenericMenu: Menú generico
/// - IndicatorsCalendary: Menú para indicadores
enum LateralMenuOptions: Int {
  case DashboardMenu = 0
  case VisionMenu
  case IndicatorsCalendary
}



// Opciones para el botón derecho de la barra de navegación
///
/// - none: Ninguna acción
/// - dependents: Botón dependientes
/// - next: Botón Siguiente
/// - selected: Botón Seleccionar
/// - search: Botón Búsqueda
enum RightButtonOption {
    case none
    case dependents
    case next
    case selected
    case search

}

/// Opciones para el botón izquierdo de la barra de navegación
///
/// - none: Ninguna acción
/// - cancel: Cancelar
enum LeftButtonOption {
  case none
  case cancel
}

// Opciones para el envío de correo eléctronico
///
/// - None: Ninguna
/// - Checklist: Checklist
/// - Accompaniment: Acompañamiento
/// - Session1to1: Sesión 1 a 1
/// - Blackboard: Pizarrón
/// - Galery: Galería
/// - Tracing: Seguimientos
enum OptionsEmail{
  case None
  case Checklist
  case Accompaniment
  case ServiceSurvey
  case Session1to1
  case Blackboard
  case Galery
  case Tracing
}


/// Orientación de una paleta de colores
///
/// - horizontal: Muestra el gradient de manera horizontal
/// - vertical: Muestra el gradient de manera vertical
enum GRADIENT {
  case horizontal
  case vertical
}

// Options view
enum OptionView: Int {
  case RCV = 1
  case NETEO
  case SAVING
  case INSURANCE
}

// Opciones para obtener el valor del archivo planes_ejecutivos.csv
enum csvOption: Int {
    case figura = 1
    case color
    case plan
    case nomina
}

/// Opciones para el segmento
///
/// - CertNAP: Certificación NAP
/// - CertAMIB: Certificación AMIB
/// - CertArt103: Certificación Art. 103
/// - CartSello: Certificación Sello SURA
/// - CertRollOver: Certificación Roll Over
enum CertificationsSegmented: Int {
    case CertNAP = 0
    case CertAMIB
    case CertArt103
    case CartSello
    case CertRollOver
}


/// Opciones de conexión
///
/// - notReachable: No hay conexión
/// - reachableViaWWAN: Conexión por red
/// - reachableViaWiFi: Conexión por WI-FI
enum ReachabilityStatus: Int {
    case notReachable = 0
    case reachableViaWWAN
    case reachableViaWiFi
}
