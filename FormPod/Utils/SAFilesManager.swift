//
//  SAFilesManager.swift
//  SmartApp
//
//  Created by Karen Sidney on 06/09/17.
//  Copyright © 2017 SURA. All rights reserved.
//
//  Clase que ayuda al manejo de archivos

import UIKit
import SSZipArchive

class SAFilesManager: NSObject {
    
    //MARK: - Guardar imagen
    
    /// Crea el directorio
    ///
    /// - Parameter nameDirectory: Nombre del directorio a crear
    class func createSubDirectoryImages(nameDirectory: String, path: String) {
        
        let documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let pathImages = documentsPath.appendingPathComponent(path)
        let logsPath = pathImages?.appendingPathComponent(nameDirectory)
        
        DLog(message: "Path del directorio: \(logsPath!)")
        do {
            try FileManager.default.createDirectory(atPath: logsPath!.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            DLog(message: "No se pudo crear el directorio: \(error.debugDescription)")
        }
    }
    
    
    
    /// Verifica si existe el directorio
    ///
    /// - Parameter nameDirectoryOrFile: Nombre del directorio
    /// - Parameter mainDirectory: Nombre de la carpeta contenedora
    /// - Returns: Regresa un boleano (si/no)
    class func existSubDirectoryImages (nameDirectoryOrFile: String, mainDirectory: String) -> Bool{
        
        let fullPath = pathSubDirectoryImages(directoryName: nameDirectoryOrFile, mainDirectory: mainDirectory)
        let fileManager = FileManager.default
        var isDir : ObjCBool = true
        if fileManager.fileExists(atPath: fullPath.path, isDirectory:&isDir) {
            if isDir.boolValue {
                DLog(message: "Existe y es un directorio")
                return true
            } else {
                DLog(message: "Existe pero no un directorio")
                return false
            }
        } else {
            DLog(message: "Archivo no existe")
            return false
        }
        
    }
    
    
    /// Obtiene la dirección del subdirectorio
    ///
    /// - Parameter directoryName: Nombre del directorio
    /// - Parameter mainDirectory: Nombre de la carpeta contenedora
    /// - Returns: Regresa la dirección del subdirectorio
    class func pathSubDirectoryImages(directoryName: String, mainDirectory: String) -> URL {
        
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let urlDocuments = URL(fileURLWithPath: path)
        let urlImages = urlDocuments.appendingPathComponent(mainDirectory) // Agrega directorio previo
        let filePath = urlImages.appendingPathComponent(directoryName).path
        
        let url = URL(fileURLWithPath:filePath)
        
        return url
    }
    
    
    /// Obtiene el subdirectorio . Sino existe lo crea
    ///
    /// - Parameter nameOfSubdirectory: Nombre del subdirectorio
    /// - Parameter mainDirectory: Nombre de la carpeta contenedora
    /// - Returns: Regresa la dirección del subdirectorio creado o existente
    class func getsSubDirectory(nameOfSubdirectory:String, directory: String) -> URL {
        
        if existSubDirectoryImages(nameDirectoryOrFile: nameOfSubdirectory, mainDirectory: directory) == false {
            createSubDirectoryImages(nameDirectory: nameOfSubdirectory, path: directory)
        }
        
        let url = pathSubDirectoryImages(directoryName: nameOfSubdirectory, mainDirectory: directory)
        return url
    }
    
    
    /// Guarda la imagen en el subdirectorio
    ///
    /// - Parameters:
    ///   - imageToSave: Image a guardar
    ///   - nameOfImage: Nombre de la imagen
    ///   - nameDirectory: Nombre de directorio
    class func saveImageIntoDirectory(imageToSave: UIImage, nameOfImage:String , nameDirectory:String){
        
        if let data = UIImagePNGRepresentation(imageToSave) {
            let directory = getsSubDirectory(nameOfSubdirectory: nameDirectory, directory: kSANameMainDirectory)
            let filename = directory.appendingPathComponent(nameOfImage)
            do {
                try data.write(to: filename)
            } catch let error as NSError {
                DLog(message: "No se pudo guardar la imagen\(error.debugDescription)")
            }
        }
        
    }
    
    
    /// Guarda la imagen en el subdirectorio y regresa la url donde se guardo
    ///
    /// - Parameters:
    ///   - imageToSave: Image a guardar
    ///   - nameOfImage: Nombre de la imagen
    ///   - nameDirectory: Nombre de directorio
    class func saveImageIntoDirectoryAndGetURL(imageToSave: UIImage, nameOfImage:String , nameDirectory:String) -> URL?{
        
        var url = URL(string:"")
        
        if let data = UIImagePNGRepresentation(imageToSave) {
            let directory = getsSubDirectory(nameOfSubdirectory: nameDirectory, directory: kSANameMainDirectory)
            url = directory.appendingPathComponent(nameOfImage)
            do {
                try data.write(to: url!)
                return url
            } catch let error as NSError {
                DLog(message: "No se pudo guardar la imagen\(error.debugDescription)")
                return url
            }
        }
        return url
        
    }
    
    //MARK: - Guardar Archivo
    
    
    /// Guarda el archivo en el subdirectorio
    ///
    /// - Parameters:
    ///   - nameOfFile: Nombre del archivo
    ///   - nameDirectory: Nombre de directorio
    ///   - tempDirectory: Carpeta temporal
    class func saveFileIntoDirectory(nameOfFile: String , nameDirectory:String, tempDirectory: String) -> Bool{
        
        if let tempURL = URL(string: tempDirectory) {
            let directory = getsSubDirectory(nameOfSubdirectory: nameDirectory, directory: kSANameFilesDirectory)
            let destination = directory.path
            let filename = directory.appendingPathComponent(nameOfFile)
            let file = filename.path
            do {
                try FileManager.default.copyItem(at: tempURL, to: filename)
                
                if SSZipArchive.unzipFile(atPath:file, toDestination: destination) {
                    try FileManager.default.removeItem(at: filename)
                    return true
                }
                
            } catch let error as NSError {
                DLog(message: "No se pudo guardar el archivo\(error.debugDescription)")
            }
        }
        
        return false
    }
    
    
    //MARK: - Galería
    
    
    /// Obtiene la lista de directorios dentro del directorio imágenes
    ///
    /// - Returns: Regresa la lista de directorios
    class func listFilesFromImagesFolder() -> [String]?
    {
        let fileMngr = FileManager.default
        
        let directory = getPathFilesDirectory(path:kSANameMainDirectory)
        
        if directory == nil {
            return []
        }else{
            do {
                var directories =  try fileMngr.contentsOfDirectory(atPath:directory!)
                for directory in directories {
                    let listFilesInDirectory: [String] = SAFilesManager.listFilesFromSubImagesFolder(nameSubDirectory: directory)!
                    if listFilesInDirectory.count == 0 {
                        SAFilesManager.deletedSubdirectory(nameDirectory: directory)
                    }
                }
                directories =  try fileMngr.contentsOfDirectory(atPath:directory!)
                return directories
            } catch let error as NSError {
                DLog(message:"No hay ningún directorio dentro de imágenes \(error.debugDescription)")
                return []
            }
        }
        
        
    }
    
    /// Obtiene el path del directorio del archivo
    ///
    /// - Returns: Regresa el Path
    class func getPathFilesDirectory(path: String) -> String?{
        
        let fullPathImageFolder: URL = pathImagesDirectory(folder: path)
        let fileManager = FileManager.default
        var isDir : ObjCBool = true
        if fileManager.fileExists(atPath: fullPathImageFolder.path, isDirectory:&isDir) {
            if isDir.boolValue {
                
                DLog(message:"Existe y es un directorio")
                return fullPathImageFolder.path
            } else {
                DLog(message:"Existe pero no un directorio")
                return nil
            }
        } else {
            DLog(message:"Archivo no existe")
            return nil
        }
        
    }
    
    
    /// Obtiene el path del directorio de principal "documents/imagenes"
    ///
    /// - Returns: Regresa el Path
    class func pathImagesDirectory(folder: String) -> URL {
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let urlDocuments = URL(fileURLWithPath: path)
        let urlImages = urlDocuments.appendingPathComponent(folder)
        
        return urlImages
    }
    
    
    /// Obtiene la lista de direcciones de cada imagen dentro del subdirectorio de imágenes
    ///
    /// - Parameter nameSubDirectory: Nombre del subdirectorio
    /// - Returns: Regresa la lista de archivos
    class func lisWithTheUrlsImageFromSubImagesFolder(nameSubDirectory: String) -> [URL]
    {
        let fileMngr = FileManager.default
        var arrayPathsImages:[URL] = []
        let directory = getsSubDirectory(nameOfSubdirectory: nameSubDirectory, directory: kSANameMainDirectory)
        do {
            let files = try fileMngr.contentsOfDirectory(atPath:directory.path)
            for nameFile in files {
                let urlImage = self.getPathImagePNG(subDirectory: nameSubDirectory, imageName: nameFile)
                arrayPathsImages.append(urlImage)
            }
            var lurls: [URL] = []
            lurls =  arrayPathsImages
            
            lurls.sort { $0.absoluteString.compare(
                $1.absoluteString, options: .numeric) == .orderedDescending
            }
            return lurls
        } catch let error as NSError {
            DLog(message:"No hay ningún archivo en el directorio\(error.debugDescription)")
            return arrayPathsImages
        }
        
    }
    
    /// Obtiene la lista de imágenes recientes
    ///
    /// - Parameter listImages: Lista de imágenes
    /// - Returns: Lista de recientes
    class func recentlyListImages(listURLs:[URL]) -> [URL]{
        
        var listImagesFinal: [URL] = []
        var listImagesOfTheAlbum: [String] = []
        
        for urlImage in listURLs {
            let urlString = urlImage.absoluteString
            listImagesOfTheAlbum.append(urlString)
        }
        
        listImagesOfTheAlbum.sort {
            $0.compare($1, options: .numeric) == .orderedDescending
        }
        
        if listImagesOfTheAlbum.count > 3 {
            let range = listImagesOfTheAlbum.index(listImagesOfTheAlbum.startIndex, offsetBy: 3)..<listImagesOfTheAlbum.endIndex
            listImagesOfTheAlbum.removeSubrange(range)// Solo obtendra las 3 primeras recientes
        }
        
        for item in listImagesOfTheAlbum {
            if let urlConvert = URL(string:item) {
                listImagesFinal.append(urlConvert)
            }
        }
        
        return listImagesFinal
    }
    
    
    /// Obtiene la lista de archivos dentro del subdirectorio de imágenes
    ///
    /// - Parameter nameSubDirectory: Nombre del subdirectorio
    /// - Returns: Regresa la lista de archivos
    class func listFilesFromSubImagesFolder(nameSubDirectory: String) -> [String]?
    {
        let fileMngr = FileManager.default
        
        let directory = getsSubDirectory(nameOfSubdirectory: nameSubDirectory, directory: kSANameMainDirectory)
        do {
            return try fileMngr.contentsOfDirectory(atPath:directory.path)
        } catch let error as NSError {
            DLog(message:"No hay ningún archivo en el directorio\(error.debugDescription)")
            return []
        }
        
    }
    
    
    /// Obtiene la lista de archivos dentro del subdirectorio de Archivos
    ///
    /// - Parameter nameSubDirectory: Nombre del subdirectorio
    /// - Returns: Regresa la lista de archivos
    class func listFilesFromSubFolder(nameSubDirectory: String) -> [String]?
    {
        let fileMngr = FileManager.default
        
        let directory = getsSubDirectory(nameOfSubdirectory: nameSubDirectory, directory: kSANameFilesDirectory)
        do {
            return try fileMngr.contentsOfDirectory(atPath:directory.path)
        } catch let error as NSError {
            DLog(message:"No hay ningún archivo en el directorio\(error.debugDescription)")
            return []
        }
        
    }
    
    
    /// Obtiene la dirección (path) completa donde se encuentra la imagen .png
    ///
    /// - Parameters:
    ///   - subDirectory: Nombre del subdirectorio
    ///   - imageName: Nombre de la imagen
    /// - Returns: Regresa el Path de la imagen .png
    class func getPathImagePNG(subDirectory:String , imageName:String) -> URL {
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let pathsWithSubDirectories = NSString.path(withComponents: [paths, kSANameMainDirectory, subDirectory])
        
        let url = URL(fileURLWithPath: pathsWithSubDirectories).appendingPathComponent(imageName)
        return url
    }
    
    /// Obtiene la dirección (path) completa donde se encuentra el archivo
    ///
    /// - Parameters:
    ///   - subDirectory: Nombre del subdirectorio
    ///   - fileName: Nombre de la imagen
    /// - Returns: Regresa el Path del archivo
    class func getPathFile(subDirectory:String , fileName:String) -> URL {
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let pathsWithSubDirectories = NSString.path(withComponents: [paths,kSANameFilesDirectory, subDirectory])
        
        let url = URL(fileURLWithPath: pathsWithSubDirectories).appendingPathComponent(fileName)
        return url
    }
    
    
    /// Obtiene la dirección (path) completa donde se encuentra la imagen .png
    ///
    /// - Parameters:
    ///   - imageName: Nombre de la imagen con .png
    /// - Returns: Regresa el Path de la imagen .png
    class func getPathImageOnlyWithName(imageWithPath:String) -> URL? {
        
        
        let onlyname = (imageWithPath as NSString).deletingPathExtension
        if let filePath = Bundle.main.path(forResource: onlyname, ofType: ".png", inDirectory: kSANameMainDirectory) {
            DLog(message: filePath)
        }
        return nil
    }
    
    /// Se borra directorio
    ///
    /// - Parameter nameDirectory: Nombre del directorio a borrar
    class func deletedSubdirectory(nameDirectory:String) {
        
        let fileManager = FileManager.default
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let pathsWithSubDirectory = NSString.path(withComponents: [paths, kSANameMainDirectory, nameDirectory])
        
        // Borrar'subfolder' folder
        do {
            try fileManager.removeItem(atPath: pathsWithSubDirectory)
        }
        catch let error as NSError {
            DLog(message:"Ocurio un error:  \(error)")
        }
    }
    
    class func deleteDirectory(nameDirectory:String) {
        let fileManager = FileManager.default
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let urlDocuments = URL(fileURLWithPath: path)
        let urlImages = urlDocuments.appendingPathComponent(nameDirectory)
        let urlFinal = URL(fileURLWithPath: urlImages.path)
        do {
            try fileManager.removeItem(atPath: urlFinal.path)
        }
        catch let error as NSError {
            DLog(message:"Ocurio un error:  \(error)")
        }
        
    }
    
    /// Obtiene la lista de álbumes recientes
    ///
    /// - Parameter list: Lista de álbumes
    /// - Returns: Lista de recientes
    class func recentlyListAlbums(list:[String]) -> [(String, String)]
    {
        
        var recently: Dictionary<String,String> = [:]
        
        for directory in list {
            var listFilesInDirectory: [String] = SAFilesManager.listFilesFromSubImagesFolder(nameSubDirectory: directory)!
            listFilesInDirectory.sort {
                $0.compare($1, options: .numeric) == .orderedDescending
            }
            DLog(message: "\(listFilesInDirectory)")
            recently[directory] = listFilesInDirectory.first
            
        }
        
        var sortedDictionary =  recently.sortedByValue
        var countRecently = 3
        if UIDevice.current.userInterfaceIdiom == .pad {
            countRecently = 3
        }else {
            countRecently = 2
        }
        
        if sortedDictionary.count > countRecently {
            let range = sortedDictionary.index(sortedDictionary.startIndex, offsetBy: countRecently)..<sortedDictionary.endIndex
            sortedDictionary.removeSubrange(range)// Solo obtendra las 3 primeras recientes
        }
        
        
        return sortedDictionary
    }
    
    /// Obtiene la lista de imágenes recientes
    ///
    /// - Parameter listImages: Lista de imágenes
    /// - Returns: Lista de recientes
    class func recentlyListImages(listImages:[String]) -> [String]{
        
        var listImagesOfTheAlbum: [String] = []
        
        listImagesOfTheAlbum = listImages
        listImagesOfTheAlbum.sort {
            $0.compare($1, options: .numeric) == .orderedAscending
        }
        
        if listImagesOfTheAlbum.count > 3 {
            let range = listImagesOfTheAlbum.index(listImagesOfTheAlbum.startIndex, offsetBy: 3)..<listImagesOfTheAlbum.endIndex
            listImagesOfTheAlbum.removeSubrange(range)// Solo obtendra las 3 primeras recientes
        }
        
        return listImagesOfTheAlbum
    }
    
    
    /// Regresa todas las imagenes de la carpeta documentos
    ///
    /// - Returns: Regresa un arreglo con URLS de las imagenes
    class func getAllImagesOfDocuments() -> [URL] {
        var listAllImages: [URL] = []
        
        let listAlbums = SAFilesManager.listFilesFromImagesFolder()!
        for album in listAlbums  {
            let listFilesByAlbum = SAFilesManager.lisWithTheUrlsImageFromSubImagesFolder(nameSubDirectory: album)
            listAllImages.append(contentsOf: listFilesByAlbum)
        }
        
        var lurls: [URL] = []
        lurls = listAllImages
        
        lurls.sort { $0.absoluteString.compare(
            $1.absoluteString, options: .numeric) == .orderedAscending
        }
        
        return lurls
    }
    
}
