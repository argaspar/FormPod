//
//  SADraggableView.swift
//  SmartApp
//
//  Created by Karen Sidney on 07/09/17.
//  Copyright © 2017 SURA. All rights reserved.
//
//  Pizarrón

import UIKit


/// Protocolo
public protocol DraggableViewDelegate {
  
  /// Regresa el texto a mostrar en la vista
  ///
  /// - Parameters:
  ///   - view: vista SADraggableView
  ///   - text: Texto a mostrar
  func deletedAndDrawText(view: SADraggableView , text:String!)
  
  /// Borra la vista
  ///
  /// - Parameter view: vista SADraggableView
  func deleteView(view: SADraggableView)
}


open class SADraggableView: UIView, UITextFieldDelegate {
  
  /// UITextField donde se agregará el texto
  let text: UITextField = UITextField()
  /// Botón cerrar vista
  fileprivate let buttonClose: UIButton = UIButton()
  /// Finalizar edición
  fileprivate let buttonFinish: UIButton = UIButton()
  /// delegado
  public var delegate: DraggableViewDelegate?
  
  override init(frame: CGRect) {
    super.init(frame: frame)

    let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(draggedView(gesture:)))
    self.isUserInteractionEnabled = true
    self.gestureRecognizers = [panRecognizer]
  
    // Background de la vista
    self.backgroundColor = UIColor.init(hex: SABackgroundColorGrayMainMenu)
    
    // Agregar  UITextView
    text.backgroundColor = UIColor.white
    text.placeholder = NSLocalizedString(kSATitleWriteText, comment: kSAStringComment)
    text.font = UIFont(name: kSAFamilyFontDINMedium, size: 16)
    text.autocorrectionType = UITextAutocorrectionType.no
    text.keyboardType = UIKeyboardType.default
    text.returnKeyType = UIReturnKeyType.done
    text.delegate = self
    text.textAlignment = NSTextAlignment.center
    text.borderStyle = UITextBorderStyle.none
    text.frame = CGRect(x: 20, y: 15, width: 200, height: 50)
    self.addSubview(text)
    
    // Botón Terminar edición
    buttonFinish.frame = CGRect(x: self.text.frame.width + 20, y: 15, width: 90, height: 50)
    buttonFinish.setTitle(NSLocalizedString(kSATitleButtonFinish, comment: kSAStringComment), for: .normal)
    buttonFinish.setTitleColor(UIColor.white, for: .normal)
    buttonFinish.titleLabel?.font = UIFont(name: kSAFamilyFontDINMedium, size: 16)
    buttonFinish.titleLabel?.adjustsFontSizeToFitWidth = true
    buttonFinish.backgroundColor = UIColor.init(hex: SAColorBottonFinishBlackboard)
    buttonFinish.addTarget(self, action: #selector(finishEdition), for: .touchUpInside)
    self.addSubview(buttonFinish)
    // Botón Cerrar vista
    buttonClose.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
    buttonClose.backgroundColor = UIColor.clear
    buttonClose.setImage(#imageLiteral(resourceName: "close").withRenderingMode(.alwaysTemplate), for: .normal)
    buttonClose.tintColor = UIColor.black
    buttonClose.addTarget(self, action: #selector(deletedView), for: .touchUpInside)
    self.addSubview(buttonClose)
    
  }

  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.backgroundColor = UIColor.init(hex: SABackgroundColorGrayMainMenu)
    self.frame = CGRect(x: 0, y: 0, width: 320 , height: 80)
  }
  
  
  /// Mueve la vista usando un gesto
  ///
  /// - Parameter gesture: UIPanGestureRecognizer
  @objc func draggedView(gesture:UIPanGestureRecognizer) {
    
    self.bringSubview(toFront: self)
    let translation = gesture.translation(in: self)
    self.center = CGPoint(x: self.center.x + translation.x, y: self.center.y + translation.y)
    gesture.setTranslation(CGPoint.zero, in: self)
  }
  
  /// Termina la edición y llama al delegado enviando el texto ingresado en el campo de texto
  @objc func finishEdition(){
    self.delegate?.deletedAndDrawText(view: self, text: text.text)
  }
  
  /// Llama al delegado para borrar la vista
  @objc func deletedView(){
    self.delegate?.deleteView(view: self)
  }
  
  
  public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
     self.text.resignFirstResponder()
    return false
  }

  
 
}



