//
//  SAUIViewControllerExtensions.swift
//  SmartApp
//
//  Created by Karen Sidney on 09/04/18.
//  Copyright © 2018 SURA. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import Firebase


extension UIViewController {
  
  
  /// Nombre de la vista
  ///
  /// - Parameters:
  ///   - screenName: Nombre de la vista
  ///   - screenClass: nombre de la clase
//  func setScreeName(screenName: String, screenClass: String) {
//    self.sendScreenView(screenName: screenName) //GOOGLE ANALYTICS
//    Analytics.setScreenName(screenName, screenClass: screenClass) // FIREBASE
//    
//  }
  
  /// Se registra nombre de la pantalla
  ///
  /// - Parameter screenName: Nombre de la vista
//  func sendScreenView(screenName : String) {
//
//    #if RELEASE
//    let tracker = GAI.sharedInstance().tracker(withTrackingId:kSASmartAppTrackingID_DEV)
//    #else
//    let tracker = GAI.sharedInstance().tracker(withTrackingId:kSASmartAppTrackingID_DEV)
//    #endif
//
//    guard let username = SATableCorporatePersistenceManager().getValueOfCorporateTable(primaryKey: kA4)?.valueItem else {
//      return
//    }
//    tracker?.set(kGAIUserId, value: username)
//    tracker?.set(kGAIScreenName, value: screenName)
//
//    let screenTracker: NSObject = GAIDictionaryBuilder.createScreenView().build()
//    tracker?.send(screenTracker as! [AnyHashable: Any])
//
//
//  }
  
  /// Seguimiento de dimenciones de Executivo en sesión, checklist, acompañamiento y encuesta
  ///
  /// - Parameter executive: SAExecutive
  ///   - flow: Flujo
//  func trackerDimensionsExecutive(executive:SAExecutive,flow:String) {
//
//
//    #if RELEASE
//    let tracker = GAI.sharedInstance().tracker(withTrackingId:kSASmartAppTrackingID_DEV)
//    #else
//    let tracker = GAI.sharedInstance().tracker(withTrackingId:kSASmartAppTrackingID_DEV)
//    #endif
//
//    guard let username = SATableCorporatePersistenceManager().getValueOfCorporateTable(primaryKey: kA4)?.valueItem else {
//      return
//    }
//
//    tracker?.set(kGAIUserId, value: username)
//
////    //Dimensiones personalizadas
////    tracker?.set(GAIFields.customDimension(for: INDEX_GERENCIA), value: executive.gerencia) //Gerencia
////    tracker?.set(GAIFields.customDimension(for: INDEX_PUESTO), value: executive.puesto) //Puesto
////
////    let figura = SATools.getValueFromCSV(nomina: executive.nomina, option: .figura)
////    if figura != "" {
////      tracker?.set(GAIFields.customDimension(for: INDEX_FIGURA), value: figura) //Figura
////    }else {
////      tracker?.set(GAIFields.customDimension(for: INDEX_FIGURA), value: executive.figura) //Figura
////    }
//
//    tracker?.set(GAIFields.customDimension(for: INDEX_FLUJO), value:flow)//Flujo
//
//    //Enviarse
//    let screenTracker: NSObject = GAIDictionaryBuilder.createScreenView().build()
//    tracker?.send(screenTracker as! [AnyHashable: Any])
//
//  }
  
  /// Seguimiento de dimenciones del Usuario
  ///
  /// - Parameter report: SAReport
  ///   - flow: Flujo
//  func trackerDimensionsForUser(report:SAReport,flow:String) {
//
//    #if RELEASE
//    let tracker = GAI.sharedInstance().tracker(withTrackingId:kSASmartAppTrackingID_DEV)
//    #else
//    let tracker = GAI.sharedInstance().tracker(withTrackingId:kSASmartAppTrackingID_DEV)
//    #endif
//
//    guard let username = SATableCorporatePersistenceManager().getValueOfCorporateTable(primaryKey: kA4)?.valueItem else {
//      return
//    }
//
//    tracker?.set(kGAIUserId, value: username)
//
//    //Dimensiones personalizadas
//    tracker?.set(GAIFields.customDimension(for: INDEX_GERENCIA), value: report.gerencia) //Gerencia
//    tracker?.set(GAIFields.customDimension(for: INDEX_PUESTO), value: report.puesto) //Puesto
//
//    let figura = SATools.getValueFromCSV(nomina: report.nomina, option: .figura)
//    if figura != "" {
//      tracker?.set(GAIFields.customDimension(for: INDEX_FIGURA), value: figura) //Figura
//    }else {
//      tracker?.set(GAIFields.customDimension(for: INDEX_FIGURA), value: report.figura) //Figura
//    }
//    tracker?.set(GAIFields.customDimension(for: INDEX_ESTRUCTURA), value: report.segmentoAten) //Estructura
//
//    //Enviarse
//    let screenTracker: NSObject = GAIDictionaryBuilder.createScreenView().build()
//    tracker?.send(screenTracker as! [AnyHashable: Any])
//
//  }
  
  
  
  /// Seguimiento de dimenciones de userid y periflID
  ///
  /// - Parameters:
  ///   - userID: MX de usuario que hizo login
  ///   - perfilID: Cadena que contiene los perfiles separados por
  ///   - flow: Flujo
//  func trackerDimensionsLogin(userID:String, perfilID:String,flow:String){
//
//    #if RELEASE
//    let tracker = GAI.sharedInstance().tracker(withTrackingId:kSASmartAppTrackingID_DEV)
//    #else
//    let tracker = GAI.sharedInstance().tracker(withTrackingId:kSASmartAppTrackingID_DEV)
//    #endif
//
//    guard let username = SATableCorporatePersistenceManager().getValueOfCorporateTable(primaryKey: kA4)?.valueItem else {
//      return
//    }
//
//    tracker?.set(kGAIUserId, value: username)
//    tracker?.set(GAIFields.customDimension(for: INDEX_USERID), value: userID) //UserId
//    tracker?.set(GAIFields.customDimension(for: INDEX_PERFILID), value: perfilID) //PerfilId
//    tracker?.set(GAIFields.customDimension(for: INDEX_FLUJO), value:flow) //Flujo
//
//    //Enviarse
//    let screenTracker: NSObject = GAIDictionaryBuilder.createScreenView().build()
//    tracker?.send(screenTracker as! [AnyHashable: Any])
//
//  }
  
  
  /// Seguimiento de dimenciones Email origen/ Email destinatario
  ///
  /// - Parameters:
  ///   - origin: Origen
  ///   - recipient: Destinatario(s)
  ///   - flow: Flujo
//  func trackerDimensionsEmail(origin:String ,recipient:String, flow:String) {
//
//    #if RELEASE
//    let tracker = GAI.sharedInstance().tracker(withTrackingId:kSASmartAppTrackingID_DEV)
//    #else
//    let tracker = GAI.sharedInstance().tracker(withTrackingId:kSASmartAppTrackingID_DEV)
//    #endif
//
//    guard let username = SATableCorporatePersistenceManager().getValueOfCorporateTable(primaryKey: kA4)?.valueItem else {
//      return
//    }
//    tracker?.set(kGAIUserId, value: username)
//    tracker?.set(GAIFields.customDimension(for: INDEX_EMAIL_DESTINATARIO), value: recipient) //EmailDestinatario
//    tracker?.set(GAIFields.customDimension(for: INDEX_EMAIL_ORIGEN), value: origin) //EmailOrigen
//    tracker?.set(GAIFields.customDimension(for: INDEX_FLUJO), value: flow)//Flujo
//
//    //Enviarse
//    let screenTracker: NSObject = GAIDictionaryBuilder.createScreenView().build()
//    tracker?.send(screenTracker as! [AnyHashable: Any])
//  }
  
  
  /// Se agrega un evento
  ///
  /// - Parameters:
  ///   - category: Categoría
  ///   - action: Descripción de la acción
  ///   - label: Etiqueta
  ///   - value: Valor
  ///   - flowIndex: Indice de Flujo
//  func trackEvent(category: String, action: String, label: String, value: NSNumber?, flowIndex:String?) {
//
//    #if RELEASE
//    let tracker = GAI.sharedInstance().tracker(withTrackingId:kSASmartAppTrackingID_DEV)
//    #else
//    let tracker = GAI.sharedInstance().tracker(withTrackingId:kSASmartAppTrackingID_DEV)
//    #endif
//
//    guard let username = SATableCorporatePersistenceManager().getValueOfCorporateTable(primaryKey: kA4)?.valueItem else {
//      return
//    }
//    tracker?.set(kGAIUserId, value: username)
//    if flowIndex != nil {
//      tracker?.set(GAIFields.customMetric(for: INDEX_FLUJO_METRICA), value: flowIndex!)
//    }
//
//    let trackDictionary = GAIDictionaryBuilder.createEvent(withCategory: category, action: action, label: label, value: value)
//    tracker?.send(trackDictionary?.build() as! [AnyHashable : Any])
//
//    Analytics.logEvent(AnalyticsEventSelectContent, parameters:[AnalyticsParameterItemID: "\(category)", AnalyticsParameterItemName: label, AnalyticsParameterContentType: action])
//
//  }
}
