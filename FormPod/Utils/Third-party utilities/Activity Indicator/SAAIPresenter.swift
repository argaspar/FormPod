//
//  SACustomAIPresenter.swift
//  SmartApp
//
//  Created by Karen on 04/09/17.
//  Copyright © 2017 SURA. All rights reserved.
//

import UIKit

public final class SAProperties {
    /// Size of activity indicator view.
    let size: CGSize
    
    /// Message displayed under activity indicator view.
    let message: String?
    
    /// Font of message displayed under activity indicator view.
    let messageFont: UIFont
    
    /// Color of activity indicator view.
    let color: UIColor
    
    /// Color of text.
    let textColor: UIColor
    
    /// Padding of activity indicator view.
    let padding: CGFloat
    
    /// Display time threshold to actually display UI blocker.
    let displayTimeThreshold: Int
    
    /// Minimum display time of UI blocker.
    let minimumDisplayTime: Int
    
    /// Background color of the UI blocker
    let backgroundColor: UIColor
    
    public init(size: CGSize? = nil,
                message: String? = nil,
                messageFont: UIFont? = nil,
                color: UIColor? = nil,
                displayTimeThreshold: Int? = nil,
                minimumDisplayTime: Int? = nil,
                backgroundColor: UIColor? = nil,
                textColor: UIColor? = nil) {
        self.size = size ?? SACustomAIView.DEFAULT_BLOCKER_SIZE
        self.message = message ?? SACustomAIView.DEFAULT_BLOCKER_MESSAGE
        self.messageFont = messageFont ?? SACustomAIView.DEFAULT_BLOCKER_MESSAGE_FONT
        self.color = color ?? SACustomAIView.DEFAULT_COLOR
        self.padding = 0
        self.displayTimeThreshold = displayTimeThreshold ?? SACustomAIView.DEFAULT_BLOCKER_DISPLAY_TIME_THRESHOLD
        self.minimumDisplayTime = minimumDisplayTime ?? SACustomAIView.DEFAULT_BLOCKER_MINIMUM_DISPLAY_TIME
        self.backgroundColor = backgroundColor ?? SACustomAIView.DEFAULT_BLOCKER_BACKGROUND_COLOR
        self.textColor = textColor ?? color ?? SACustomAIView.DEFAULT_TEXT_COLOR
    }
}

public final class SACustomAIPresenter {
    private enum State {
        case waitingToShow
        case showed
        case waitingToHide
        case hidden
    }
    
    private let restorationIdentifier = "SACustomAIViewContainer"
    private let messageLabel: UILabel = {
        let label = UILabel()
        
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var state: State = .hidden
    private let startAnimatingGroup = DispatchGroup()
    
    public static let sharedInstance = SACustomAIPresenter()
    
    private init() {}
    
    
    // MARK: - Public interface
    
    public final func startAnimating(_ data: SAProperties) {
        guard state == .hidden else { return }
        
        state = .waitingToShow
        startAnimatingGroup.enter()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(data.displayTimeThreshold)) {
            guard self.state == .waitingToShow else {
                self.startAnimatingGroup.leave()
                
                return
            }
            
            self.show(with: data)
            self.startAnimatingGroup.leave()
        }
    }
    
    public final func stopAnimating() {
        _hide()
    }

    public final func setMessage(_ message: String?) {
        guard state == .showed else {
            startAnimatingGroup.notify(queue: DispatchQueue.main) {
                self.messageLabel.text = message
            }
            
            return
        }
        
        messageLabel.text = message
    }
    
    // MARK: - Helpers
    
    private func show(with properties: SAProperties) {
        let containerView = UIView(frame: UIScreen.main.bounds)
        
        containerView.backgroundColor = properties.backgroundColor
        containerView.restorationIdentifier = restorationIdentifier
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        let activityIndicatorView = SACustomAIView(
            frame: CGRect(x: 0, y: 0, width: properties.size.width, height: properties.size.height),
            color: properties.color,
            padding: properties.padding)
        
        activityIndicatorView.startAnimating()
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(activityIndicatorView)
        
        // Add constraints for `activityIndicatorView`.
        ({
            let xConstraint = NSLayoutConstraint(item: containerView, attribute: .centerX, relatedBy: .equal, toItem: activityIndicatorView, attribute: .centerX, multiplier: 1, constant: 0)
            let yConstraint = NSLayoutConstraint(item: containerView, attribute: .centerY, relatedBy: .equal, toItem: activityIndicatorView, attribute: .centerY, multiplier: 1, constant: 0)
            
            containerView.addConstraints([xConstraint, yConstraint])
            }())
        
        messageLabel.font = properties.messageFont
        messageLabel.textColor = properties.textColor
        messageLabel.text = properties.message
        containerView.addSubview(messageLabel)
        
        // Add constraints for `messageLabel`.
        ({
            let leadingConstraint = NSLayoutConstraint(item: containerView, attribute: .leading, relatedBy: .equal, toItem: messageLabel, attribute: .leading, multiplier: 1, constant: -8)
            let trailingConstraint = NSLayoutConstraint(item: containerView, attribute: .trailing, relatedBy: .equal, toItem: messageLabel, attribute: .trailing, multiplier: 1, constant: 8)
            
            containerView.addConstraints([leadingConstraint, trailingConstraint])
            }())
        ({
            let spacingConstraint = NSLayoutConstraint(item: messageLabel, attribute: .top, relatedBy: .equal, toItem: activityIndicatorView, attribute: .bottom, multiplier: 1, constant: 8)
            
            containerView.addConstraint(spacingConstraint)
            }())
        
        guard let keyWindow = UIApplication.shared.keyWindow else { return }
        
        keyWindow.addSubview(containerView)
        state = .showed
        
        // Add constraints for `containerView`.
        ({
            let leadingConstraint = NSLayoutConstraint(item: keyWindow, attribute: .leading, relatedBy: .equal, toItem: containerView, attribute: .leading, multiplier: 1, constant: 0)
            let trailingConstraint = NSLayoutConstraint(item: keyWindow, attribute: .trailing, relatedBy: .equal, toItem: containerView, attribute: .trailing, multiplier: 1, constant: 0)
            let topConstraint = NSLayoutConstraint(item: keyWindow, attribute: .top, relatedBy: .equal, toItem: containerView, attribute: .top, multiplier: 1, constant: 0)
            let bottomConstraint = NSLayoutConstraint(item: keyWindow, attribute: .bottom, relatedBy: .equal, toItem: containerView, attribute: .bottom, multiplier: 1, constant: 0)
            
            keyWindow.addConstraints([leadingConstraint, trailingConstraint, topConstraint, bottomConstraint])
            }())
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(properties.minimumDisplayTime)) {
            self._hide()
        }
    }
    
    private func _hide() {
        if state == .waitingToHide {
            hide()
        } else if state != .hidden {
            state = .waitingToHide
        }
    }
    
    private func hide() {
        guard let keyWindow = UIApplication.shared.keyWindow else { return }
        
        for item in keyWindow.subviews
            where item.restorationIdentifier == restorationIdentifier {
                item.removeFromSuperview()
        }
        state = .hidden
    }
}
