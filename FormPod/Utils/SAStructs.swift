//
//  Structs.swift
//  SmartApp
//
//  Created by Karen Sidney on 04/09/17.
//  Copyright © 2017 SURA. All rights reserved.
//
//  Se declaran estructuras que se usaran en el proyecto

import UIKit

/// Variable global
/// Se utiliza para cambiar el nombre de la pantalla
struct AppVariables {
  
  /// Nombre de la pantalla
  static var nameOfScreen = ""
  /// Nombre de la persona visualizada
  static var nameOfPersonView = ""
  /// Nomina 
  static var paysheetForScreen = ""

}

