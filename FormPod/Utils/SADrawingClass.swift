
//
//  SADrawingView.swift
//  SmartApp
//
//  Created by Karen on 04/09/17.
//  Copyright © 2017 SURA. All rights reserved.
//
//  Clase que dibuja en canvas

 
import UIKit

 public protocol DrawingViewDelegate {
  
    /// Empezó el dibujo
    ///
    /// - Parameter view: vista SADrawingView
    func didBeginDrawing(view: SADrawingView)
  
    /// Esta dibujando
    ///
    /// - Parameter view: vista SADrawingView
    func isDrawing(view: SADrawingView)
  
    /// Hizo el dibujo final
    ///
    /// - Parameter view: vista SADrawingView
    func didFinishDrawing(view: SADrawingView)
  
    /// Hizo Cancelar el dibujo
    ///
    /// - Parameter view: vista SADrawingView
    func didCancelDrawing(view: SADrawingView)
 }
 
 
 
 open class SADrawingView: UIView {

    /// Color de línea
    public var lineColor: UIColor = UIColor.black
    /// Anchura de línea
    public var lineWidth: CGFloat = 6.0
    /// Opacidad de la línea
    public var lineOpacity: CGFloat = 1.0
    //Utilizado para acciones de zoom y texto
    public var drawingEnabled: Bool = true
    /// Delegado
    public var delegate: DrawingViewDelegate?
    /// punto actual
    fileprivate var currentPoint: CGPoint = CGPoint()
    /// punto anterior
    fileprivate var previousPoint: CGPoint = CGPoint()
    /// punto anterior anterior
    fileprivate var previousPreviousPoint: CGPoint = CGPoint()
    /// Arreglo de caminos
    fileprivate var pathArray: [Line] = []
    /// Arreglo para rehacer
    fileprivate var redoArray: [Line] = []
    /// Tipo de herramienta
    var toolType: Int = 0
    /// PI
    fileprivate let π: CGFloat = .pi
    /// Rehacer la fuerza
    fileprivate let forceSensitivity: CGFloat = 4.0
    /// Texto a dibujar
    fileprivate var textToDraw: String = ""

  
    /// Línea
    fileprivate struct Line {
      
        var path: CGMutablePath
        var color: UIColor
        var width: CGFloat
        var opacity: CGFloat
        var associatedTool: Int = 0
        
        init(path : CGMutablePath, color: UIColor, width: CGFloat, opacity: CGFloat, tool: Int) {
            self.path = path
            self.color = color
            self.width = width
            self.opacity = opacity
            self.associatedTool = tool
        }
    }
  
  
    /// Color del texto
    private struct TextColor {
      var color: UIColor
      
      init(color: UIColor) {
        self.color = color
      }
    }
  
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = UIColor.clear
    }
    
    override open func draw(_ rect: CGRect) {
 
        let context : CGContext = UIGraphicsGetCurrentContext()!
   
      
        for line in pathArray {
          
            context.setLineWidth(line.width)
            context.setAlpha(line.opacity)
            context.setLineCap(.round)
          
            switch line.associatedTool {
            case 0:  // Pluma
                DLog(message: "Herramienta de Pluma")
                context.setStrokeColor(line.color.cgColor)
                context.addPath(line.path)
                context.setBlendMode(.normal)
                break
            
            case 1:
                DLog(message:"Herramienta de borrado")
                context.setStrokeColor(UIColor.clear.cgColor)
                context.addPath(line.path)
                context.setBlendMode(.clear)
                
                break
                
            case 3:
                DLog(message:"Herramienta multiple")
                context.setStrokeColor(line.color.cgColor)
                context.addPath(line.path)
                context.setBlendMode(.multiply)
              break
            case 4:
                if toolType == 4 {
                   context.setBlendMode(.normal)
                    DLog(message:"Herramienta texto");
                   drawText(textToDraw: textToDraw, inFrame: CGRect(x: currentPoint.x, y: currentPoint.y, width: 210, height: 80), context: context)
                }
              break
            default:
                break
            }

            context.strokePath()

        }
    }

    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard drawingEnabled == true else {
            return
        }
        
        self.delegate?.didBeginDrawing(view: self)
      if let touch = touches.first as UITouch? {
            previousPoint = touch.previousLocation(in: self)
            previousPreviousPoint = touch.previousLocation(in: self)
            currentPoint = touch.location(in: self)
            
            let newLine = Line(path: CGMutablePath(), color: self.lineColor, width: self.lineWidth, opacity: self.lineOpacity, tool:self.toolType)
            newLine.path.addPath(createNewPath())
            pathArray.append(newLine)
        }
    }
    
    override open func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard drawingEnabled == true else {
            return
        }
        
        self.delegate?.isDrawing(view: self)
      if let touch = touches.first as UITouch? {
            previousPreviousPoint = previousPoint
            previousPoint = touch.previousLocation(in: self)
            currentPoint = touch.location(in: self)
            
            let newLine = createNewPath()
            if let currentPath = pathArray.last {
                currentPath.path.addPath(newLine)
            }
        }
    }
    
    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard drawingEnabled == true else {
            return
        }
      
        self.delegate?.didCancelDrawing(view: self)
        
    }
    
    override open func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard drawingEnabled == true else {
            return
        }
        
        
    }
    
    
    // MARK: - Acciones
  
    /// Puede deshacer
    ///
    /// - Returns: Boleano (si/no)
    public func canUndo() -> Bool {
        if pathArray.count > 0 {return true}
        return false
    }
  
  
    /// Puede rehacer
    ///
    /// - Returns: Boleano (si/no)
    public func canRedo() -> Bool {
        return redoArray.count > 0
    }
    
  
    /// Deshacer
    public func undo() {
        if pathArray.count > 0 {
            redoArray.append(pathArray.last!)
            pathArray.removeLast()
        }
        
        setNeedsDisplay()
    }
  
    /// Rehacer
    public func redo() {
        if redoArray.count > 0 {
            pathArray.append(redoArray.last!)
            redoArray.removeLast()
        }
        setNeedsDisplay()
    }
  
    /// Limpiar vista
    public func clearCanvas() {
        pathArray = []
        setNeedsDisplay()
    }
  

  
    /// Crea un nuevo camino
    ///
    /// - Returns: Regresa el camino creado
    fileprivate func createNewPath() -> CGMutablePath {
 
        let midPoints = getMidPoints()
        let subPath = createSubPath(midPoints.0, mid2: midPoints.1)
        let newPath = addSubPathToPath(subPath)
        return newPath
    }
  
    /// Calcula los puntos
    private func calculateMidPoint(_ p1 : CGPoint, p2 : CGPoint) -> CGPoint {
        return CGPoint(x: (p1.x + p2.x) * 0.5, y: (p1.y + p2.y) * 0.5);
    }
    /// Obtiene los puntos
    private func getMidPoints() -> (CGPoint,  CGPoint) {
        let mid1 : CGPoint = calculateMidPoint(previousPoint, p2: previousPreviousPoint)
        let mid2 : CGPoint = calculateMidPoint(currentPoint, p2: previousPoint)
        return (mid1, mid2)
    }
  
    /// Crea un sub camino
    private func createSubPath(_ mid1: CGPoint, mid2: CGPoint) -> CGMutablePath {
        let subpath : CGMutablePath = CGMutablePath()
        subpath.move(to: CGPoint(x: mid1.x, y: mid1.y))
        subpath.addQuadCurve(to: CGPoint(x: mid2.x, y: mid2.y), control: CGPoint(x: previousPoint.x, y: previousPoint.y))
        return subpath
    }
     /// Agrega los subcaminos
    private func addSubPathToPath(_ subpath: CGMutablePath) -> CGMutablePath {
        let bounds : CGRect = subpath.boundingBox
        
        let drawBox : CGRect = bounds.insetBy(dx: -0.54 * lineWidth, dy: -0.54 * lineWidth)
        self.setNeedsDisplay(drawBox)
        return subpath
    }
  
  
    /// Crea un campo del texto y lo agrega a la vista
    public func createdTextView(){
      
      let view = SADraggableView(frame: CGRect(x: self.center.x - (320/2), y: 100, width: 320, height: 80))
      view.delegate = self
      view.text.textColor = self.lineColor
      self.addSubview(view)
 
    }
  
  
    /// Dibuja el texto en la vista
    ///
    /// - Parameters:
    ///   - viewText: Vista Que contiene el texto (SADraggableView)
    ///   - textDraw: Texto a mostrar
    func createText(viewText:SADraggableView, textDraw: String) {

        let point = CGPoint(x: viewText.frame.origin.x , y: viewText.frame.origin.y)
        drawingEnabled = false // Permite no dibujar
        currentPoint = point
        textToDraw = textDraw
        toolType = 4
        // Se crea el path
        DLog(message:"Crea la línea de Pintar texto")
        let newLine = Line(path: CGMutablePath(), color: self.lineColor, width: self.lineWidth, opacity: self.lineOpacity, tool:self.toolType)
        newLine.path.addPath(createNewPath())
        pathArray.append(newLine)
        
        self.setNeedsDisplay(viewText.frame) // Actualizar solo el frame que se indica de la vista, para que llame a drawRect y no borre lo ya dibujado
      
      
    }
  
    /// Dibuja el texto
    ///
    /// - Parameters:
    ///   - textToDraw: Texto a dibujar
    ///   - frameRect: Frame
    ///   - currentContext: contexto actual
    public func drawText(textToDraw: String, inFrame frameRect: CGRect, context currentContext:CGContext) {
      
      let paragraphStyle = NSMutableParagraphStyle()
      paragraphStyle.alignment = .center
      
      let textFont: UIFont = UIFont(name: kSAFamilyFontDINBold, size:16)!
      let textColor: UIColor = self.lineColor
      
      var attributes = [NSAttributedStringKey : Any]()
      
      attributes = [.foregroundColor: textColor,
                    .font: textFont,
                    .paragraphStyle: paragraphStyle]
      

   
      let stringRef = textToDraw as NSString
      
      stringRef.draw(in:frameRect, withAttributes: attributes)
      
     
    }
  
  
}

// MARK: - Delagados
 extension SADrawingView: DraggableViewDelegate{

   public func deletedAndDrawText(view: SADraggableView, text:String!) {
    
      createText(viewText: view, textDraw:text)
      view.removeFromSuperview()
      self.delegate?.didCancelDrawing(view: self)
    }
  
    public func deleteView(view: SADraggableView) {
      self.delegate?.didCancelDrawing(view: self)
      view.removeFromSuperview()
    }

  
 }
 
 
 

