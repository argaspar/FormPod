//
//  SAHamburgerView.swift
//  SmartApp
//
//  Created by Karen Sidney on 07/11/17.
//  Copyright © 2017 SURA. All rights reserved.
//
//  Crea el botón del menú hamburguesa
import UIKit

class HamburgerView: UIView {
  
  /// Imagen para el botón de menú hamburguesa
  let imageView: UIImageView! = UIImageView(image: UIImage(named: "Menu_ButtonMenu"))
  
  
  public required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
    configure()
  }
  
  required override init(frame: CGRect) {
    super.init(frame: frame)
    configure()
  }
  
  // MARK: RotatingView
  
  /// Rotación del menú
  ///
  /// - Parameter fraction: Rotación
  func rotate(_ fraction: CGFloat) {
    let angle = Double(fraction) *  Double.pi/2
    imageView.transform = CGAffineTransform(rotationAngle: CGFloat(angle))
  }
  
  // MARK: Private
  fileprivate func configure() {
    imageView.contentMode = UIViewContentMode.center
    imageView.frame = CGRect(x: 0 , y:0, width: self.frame.width, height: self.frame.height)
    addSubview(imageView)
  }
  
}

