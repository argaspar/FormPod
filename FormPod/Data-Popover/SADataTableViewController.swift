//
//  SADataTableViewController.swift
//  SmartApp
//
//  Created by Karen Sidney on 06/10/17.
//  Copyright © 2017 SURA. All rights reserved.
//
//  Se desplegará una tabla con una lista de strings

import UIKit

/// Protocolo
public protocol DataDelegate {
  
  /// Envia al delegado el texto seleccionado
  ///
  /// - Parameter typeSelected: texto seleccionado
  func getSelectedString(typeSelected:String)
}


class SADataTableViewController: UITableViewController {
  
    /// Delegado
    var delegatePopover: DataDelegate?
    /// Lista de opciones
    var listOptions:[String] = []
    /// Tag
    var optionTag: Int = 0
  
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        self.view.backgroundColor = UIColor.init(hex: SAColor00226C)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOptions.count
    }

  
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = self.listOptions[(indexPath as NSIndexPath).row]
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.font = UIFont(name: kSAFamilyFontDINRegular, size: 14)
        cell.backgroundColor = UIColor.init(hex: SAColor00226C)
        return cell
    }
  
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      tableView.deselectRow(at: indexPath, animated: true)
      let selected = self.listOptions[(indexPath as NSIndexPath).row]
      self.delegatePopover?.getSelectedString(typeSelected: selected)
      self.dismiss(animated: true, completion: nil)
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return 30
    }
    
}
