//
//  SAInformationViewController.swift
//  SmartApp
//
//  Created by Karen Sidney on 07/10/17.
//  Copyright © 2017 SURA. All rights reserved.
//
// Muestra una vista con una etiqueta para mostrar información

import UIKit

class SAInformationViewController: UIViewController {

    /// Etiqueta a mostrar
    @IBOutlet weak var labelInformation: UILabel!
    /// Texto a mostrar
    var information = ""
  
    override func viewDidLoad() {
      super.viewDidLoad()
      
      self.view.backgroundColor = UIColor.init(hex: SAColor00226C)
      labelInformation.text = information
      labelInformation.textColor = UIColor.white
      labelInformation.font = UIFont.init(name: kSAFamilyFontDINRegular, size: 12.0)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
