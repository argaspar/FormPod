//
//  SASlateViewController.swift
//  FormPod
//
//  Created by Rafael Gaspar on 17/05/18.
//  Copyright © 2018 NutSystem. All rights reserved.
//

import UIKit

class SASlateViewController: UIViewController {
    
    @IBOutlet weak var prospectusTableview: UITableView!
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var projectedHeight: NSLayoutConstraint!
    @IBOutlet weak var mesesLbl: UILabel!
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    @IBOutlet weak var dependienteTxt: SkyFloatingLabelTextField!
    
    // Screen height.
    var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    
    var prospectus = ["Fernando Píneda Ceron", "Araleci Perez Melendez", "Alejandra Martinez", "Pamela Benitez", "Laura Miranda Pichardo", "Raul Fernandez Valderrama", "Rafael Omar Filoteo Gama", "Hector Alberto Gonzalez Chacon"]
    var dates = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (screenHeight == 568.0){
            height.constant = 450.0
            projectedHeight.constant = -460.0
            heightTable.constant = 233
        }
        if UIDevice.current.userInterfaceIdiom == .pad {
            height.constant = 580.0
            projectedHeight.constant = -590.0
            heightTable.constant = 380
        }
        self.obtainMonths()
        
       
        self.prospectusTableview.delegate = self
        self.prospectusTableview.dataSource = self
        self.prospectusTableview.tableFooterView = UIView()
        addGestureRecognizer()
    }
    
    func obtainMonths(){
        SATools.getCurrentDate(completion: {year, month, day in
            let vecesInteraccion = month + 12
            var monthWithYear = ""
            for i in (month...vecesInteraccion).reversed() {
                if(i > 12){
                    monthWithYear = SATools.monthNameByNumber(month: (i - 12))
                    self.dates.append((monthWithYear + " \(year)") )
                }else{
                    monthWithYear = SATools.monthNameByNumber(month: (i))
                    self.dates.append((monthWithYear + " \(year - 1)"))
                }
            }
        })
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.dependienteTxt.textAlignment = .center
        self.dependienteTxt.titleLabel.textAlignment = .left

    }
    
    func addGestureRecognizer(){
        let tapGesture = UITapGestureRecognizer (target: self, action: #selector(callToShowMonthsPicker(_:)))
        tapGesture.numberOfTapsRequired = 1
        self.mesesLbl.addGestureRecognizer(tapGesture)
        
        let tapDependienteTxt = UITapGestureRecognizer (target: self, action: #selector(callToShowDependentsPicker(_:)))
        tapDependienteTxt.numberOfTapsRequired = 1
        self.dependienteTxt.addGestureRecognizer(tapDependienteTxt)
    }
    
    @objc func callToShowMonthsPicker(_ sender: UITapGestureRecognizer){
        monthsPicker(sender: sender)
    }
    
    @objc func callToShowDependentsPicker(_ sender: UITapGestureRecognizer){
        showDependents(sender: sender)
    }
    
    
    ///Configuracion picker para mostrar los meses
    ///
    /// - Parameter sender: Tap
    func showDependents(sender: UITapGestureRecognizer){
        
        let title = "Selecciona un mes:"
        let alertVC = UIAlertController(title: title, message: "\n\n\n\n\n\n\n\n\n", preferredStyle: .actionSheet)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alertVC.view.bounds = CGRect.init(x: 0, y: 0, width: 300, height: 300)
        }else{
            alertVC.view.bounds = CGRect.init(x: 0, y: 0, width: 300, height: 300)
        }
        
        
        let pickerView = UIPickerView.init(frame: CGRect.init(x: 0, y: 50, width:  alertVC.view.bounds.size.width, height: 150))
        
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = 1
        alertVC.view.addSubview(pickerView)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alertVC.modalPresentationStyle = .popover
            alertVC.popoverPresentationController?.sourceRect = (sender.view?.bounds)!
            alertVC.popoverPresentationController?.sourceView = sender.view
        }
        present(alertVC, animated: true, completion: {
            pickerView.frame.size.width = alertVC.view.frame.size.width
        })
        let okAction: UIAlertAction = UIAlertAction(title: "OK", style: .default) { action -> Void in
            alertVC.dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(okAction)
        
    }
    
    ///Configuracion picker para mostrar los meses
    ///
    /// - Parameter sender: Tap
    func monthsPicker(sender: UITapGestureRecognizer){
        
        let title = "Selecciona un dependiente:"
        let alertVC = UIAlertController(title: title, message: "\n\n\n\n\n\n\n\n\n", preferredStyle: .actionSheet)

        alertVC.view.bounds = CGRect.init(x: 0, y: 0, width: 300, height: 300)
        
        let pickerView = UIPickerView.init(frame: CGRect.init(x: 0, y: 50, width:  alertVC.view.bounds.size.width, height: 150))
        
        pickerView.delegate = self
        pickerView.dataSource = self
        alertVC.view.addSubview(pickerView)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alertVC.modalPresentationStyle = .popover
            alertVC.popoverPresentationController?.sourceRect = (sender.view?.bounds)!
            alertVC.popoverPresentationController?.sourceView = sender.view
        }
        present(alertVC, animated: true, completion: {
            pickerView.frame.size.width = alertVC.view.frame.size.width
        })
        let okAction: UIAlertAction = UIAlertAction(title: "OK", style: .default) { action -> Void in
            alertVC.dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(okAction)
        
    }
    
    
    @IBAction func addNewProspectus(_ sender: Any) {
        let storyboardGroupMeeting = UIStoryboard(name: "Slate" , bundle: nil)
        let initialViewController:SASlateFormViewController = storyboardGroupMeeting.instantiateViewController(withIdentifier: "formStoryBoard") as! SASlateFormViewController
        splitViewController?.showDetailViewController(initialViewController, sender: nil)
    }
    
}

extension SASlateViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(prospectus.count == 0){
            self.prospectusTableview.setEmptyMessage("No hay Proyectados Capturados")
        }else{
            self.prospectusTableview.restore()
        }
        return prospectus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = prospectus[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title:  nil, handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.prospectus.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            //self.prospectusTableview.reloadData()
            success(true)
        })
        deleteAction.image = UIImage(named: "Trash")
        deleteAction.backgroundColor = .red
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    
}

// MARK: - Delegado del Picker
extension SASlateViewController: UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView.tag == 1){
            return prospectus.count
        }else{
            return dates.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGFloat(40)
        }else {
            return CGFloat(30)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label: UILabel = UILabel()
        if UIDevice.current.userInterfaceIdiom == .pad {
            label.font = UIFont (name: kSAFamilyFontDINRegular, size: 28)
        }else {
            label.font = UIFont (name: kSAFamilyFontDINRegular, size: 24)
        }
        if(pickerView.tag == 1){
             label.text = prospectus[row]
        }else{
            label.text = dates[row]
        }
        label.textAlignment = .center
        return label
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 1){
            self.dependienteTxt.text = prospectus[row]
            self.dependienteTxt.textAlignment = .center
            self.dependienteTxt.titleLabel.textAlignment = .left
        }else{
            self.mesesLbl.text = dates[row]
            self.mesesLbl.textAlignment = .center
        }
        
    }
}


