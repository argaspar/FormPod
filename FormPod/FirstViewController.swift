//
//  FirstViewController.swift
//  FormPod
//
//  Created by Rafael Gaspar on 30/05/18.
//  Copyright © 2018 NutSystem. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    @IBAction func showSplitView(_ sender: Any) {
        let storyboardGroupMeeting = UIStoryboard(name: "Slate" , bundle: nil)
        let initialViewController:SASlateFormViewController = storyboardGroupMeeting.instantiateViewController(withIdentifier: "formStoryBoard") as! SASlateFormViewController
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    
}
